// Для гребаного хрома
var c_i = new Image(); c_i.src = '/' + DIR + 'img/logo-b.png';
var c_ii = new Image(); c_ii.src = '/' + DIR + 'img/title-hr-b.png';

// А это закрывает к ебаной матери подпись на слайде по нажатию ECS, если не открыто
// всплывающего окна
$(document).on('keyup keypress', function(event)
{
    // Нажата ESC
    if(event.keyCode === 27)
    {
        // Не открыто всплывающих окон (сначала ESC закрывает окно)
        if( ! $('.popup:visible').size())
        {
            var $scr = onepageScroll._getCurrentScreen();

            if($scr.find('.text.visible').size())
            {
                $('footer .btn-text').trigger('click');
            }
        }
    }
});





// Скорость прокрутки страницы
var SCROLL_SPEED = 800;

// Проверка браузера
var browser =
{
    // Любая из версий IE
    ie: function()
    {
        return (!!window.ActiveXObject) || window.navigator.msPointerEnabled;
    },
    // Opera
    opera: function()
    {
        return !!window.opera || /opera|opr/i.test(navigator.userAgent);
    }
};

// Проверка, имеет ли устройство сенсорный экран
function isTouchDevice()
{
    return 'ontouchstart' in window;
}



// Всплывающие окна
var popup =
{
    // Открывает окно, принимая в качестве аргумента имя этого окна
    _open: function(cls)
    {
        $('.popup.' + cls + ' .popup-content').css(
            { left: -80, opacity: 0 }
        );

        $('.popup.' + cls).fadeIn(500, function()
        {
            $('.popup.' + cls + ' .popup-content').animate(
                {
                    left: 0,
                    opacity: 1
                },
                500,
                'easeOutSine'
            );
        });

        // Вешаем прослушиватели событий для управления и подгона размеров окна
        $(window).on('keyup keypress', popup._keyHandler);
        $(window).on('resize', popup._resize);

        // Скрываем стрелки переключения глав и стрелку прокрутки страницы наверх, чтобы не
        // болтались на заднем плане
        $('.scroll-top, .change-chapter').addClass('hidden-lock');

        // Скрываем кнопки выбора слайда в футере
        $('footer nav').fadeOut(300);

        // Подгоняем размеры контента окна под окно браузера
        popup._resize();
    },
    // Закрывает любое отображаемое окно и открывает то окно, чье имя было передано в качестве аргумента
    _closeOther: function(popupType)
    {
        // Окно, отображаемое на данный момент
        var $openedPopup = $('.popup:visible');

        // Окно открыто
        if($openedPopup.size())
        {
            // Закрыть окно
            popup.close();

            // Открыть окно в случае, если открываемое и только что закрытое окно одно и то же
            if( ! $openedPopup.hasClass('popup-' + popupType))
            {
                // Ждем, пока закроется старое окно
                setTimeout(function()
                {
                    // Эмулируем клик по кнопке на нижней панели, чтобы открыть сответствующее ей окно
                    $('footer .btn-' + popupType).trigger('click');
                }, 1000);
            }

            return false;
        }
        // Нет открытых окон
        else
        {
            // Вернуть true, позволив функции, вызвавшей этот метод, самой открыть окно
            return true;
        }
    },
    // Подгоняет размеры контента в окне в зависимости от размеров окна браузера
    _resize: function()
    {
            // Контейнер текущей отображаемой вкладки
        var $popupContent = $('.popup:visible .popup-content:visible'),
            // Прокручиваемый контейнер с контентом
            $scroll = $popupContent.find('.scroll:visible'),
            // Высота окна браузера
            winH = $(window).height();

        var positionTop = parseInt($popupContent.css('marginTop'));

        $scroll.css('height', winH - positionTop - 75);
    },
    // Обработчик нажатий клавиш клавиатуры
    _keyHandler: function(event)
    {
        if(event.keyCode == 27 || event.charCode == 105 || event.charCode == 120 || event.charCode == 1095 || event.charCode == 1096)
        {
            event.preventDefault();
            // Закрыть всплывающее окно
            popup.close();
        }
    },

    // Для "О проекте"
    about:
    {
        // Отображает окно "О проекте"
        show: function(target)
        {
            // Если не было открыто другое окно
            if(popup._closeOther('about'))
            {
                // При нажатии на кнопку, "О проекте" не было открыто
                if( ! $(target).hasClass('active'))
                {
                    // Открываем окно
                    popup._open('popup-about');
                    // Выбираем самый первый раздел
                    popup.about.select('about');
                    // Подсвечиваем кнопку оглавления на нижней панели
                    $('footer .btn-popup').removeClass('active');
                    $(target).addClass('active');
                }
                // Если оглавление уже открыто
                else
                {
                    // Скрываем его
                    popup.close();
                }
            }
        },
        // Выбор раздела в окне "О проекте"
        select: function(tab)
        {
                // Выбранный пункт списка слева
            var $tabLi   = $('.popup.popup-about menu li.' + tab),
                // Текст выбранного раздела
                $tabText = $('.popup.popup-about .text.text-' + tab);

            // Если раздел, который требуется отобразить, в данный момент не открыт
            if( ! $tabLi.hasClass('active'))
            {
                // Выделить открываемый раздел в списке
                $tabLi.siblings().removeClass('active');
                $tabLi.addClass('active');

                // Скрыть текущий открытый раздел
                $('.popup.popup-about .text:visible').fadeOut(300, function()
                {
                    // Открыть новый раздел
                    $tabText.fadeIn(300);
                    // Подогнать размеры в соответствии с окном браузера
                    popup._resize();
                });
            }
            // Попытка открыть уже открытый раздел
            else
            {
                // Подогнать размеры в соответствии с окном браузера
                popup._resize();
            }
        }
    },
    // Для оглавления
    tableContents:
    {
        // Отображает окно оглавления
        show: function(target)
        {
            // Если не было открыто другое окно
            if(popup._closeOther('table-of-contents'))
            {
                // При нажатии на кнопку, оглавление не было открыто
                if( ! $(target).hasClass('active'))
                {
                    // Открываем окно
                    popup._open('popup-table-of-contents');
                    // Подсвечиваем кнопку оглавления на нижней панели
                    $('footer .btn-popup').removeClass('active');
                    $(target).addClass('active');
                }
                // Если оглавление уже открыто
                else
                {
                    // Скрываем его
                    popup.close();
                }
            }
        }
    },
    // Зкрывает открытое всплывающее окно
    close: function()
    {
        // Закрытие открытого окна
        $('.popup .popup-content').animate(
            {
                left: 80,
                opacity: 0
            },
            500,
            'easeOutSine',
            function()
            {
                // Исчезновение полупрозрачного фона
                $('.popup').delay(200).fadeOut(500);
                // Отображаем стрелки переключения глав и прокрутки страницы наверх
                $('.scroll-top, .change-chapter').removeClass('hidden-lock');
            }
        );

        // Убираем выделение кнопки окна на нижней панели
        $('footer .btn-popup').removeClass('active');

        // Отображаем скрытые кнопки выбора слайда в футере
        $('footer nav').delay(800).fadeIn(300);

        // Убираем прослушиватели событий, они больше не нужны
        $(window).off('keyup keypress', popup._keyHandler);
        $(window).off('resize', popup._resize);
    }
};

// Включает/выключает полноэкранный режим браузера
function fullScreenToggle()
{
    // Ишак ведет себя неадекватно, ему нельзя переключаться в фуллскрин
    if(browser.ie()) return;

    // Полноэкранный режим уже активирован, выбираем поддерживаемый браузером
    // мтеод, чтобы вернуться в обычный режим
    if(document.mozFullScreen || document.fullScreen || document.webkitIsFullScreen)
    {
        if(document.cancelFullScreen)
            document.cancelFullScreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitCancelFullScreen)
            document.webkitCancelFullScreen();
        else if(document.exitFullscreen)
            document.exitFullscreen();
    }
    // Полноэкранный режим не активирован, выбираем поддерживаемый браузером
    // мтеод, чтобы развернуть окно на весь экран
    else
    {
        var doc = document.documentElement;

        if (doc.requestFullScreen)
            doc.requestFullScreen();
        else if (doc.mozRequestFullScreen)
            doc.mozRequestFullScreen();
        else if (doc.webkitRequestFullScreen)
            doc.webkitRequestFullScreen();
    }
}


var onepageScroll =
{
    // Ширина и высота окна браузера
    winW: 0,
    winH: 0,
    // Номер текущего слайда
    currentScreen: 1,
    // Номер текущей главы
    currentChapter: 1,
    // Общее число глав на лендинге
    countChapters: null,
    // Флаг отображения всех надписей на слайдах с фотографиями
    textGlobalVisible: false,
    // Массив с данными о положении слайдов текущей главы относительно начала страницы
    sectionsPos: [],
    // Флаг блокировки полеса мыши
    scrollLock: false,
    // Флаг, что видео было запущено
    videoPlayed: false,

    // Обработчик события изменения размеров окна браузера
    _resizeHandler: function()
    {
        // Сохраняем в свойствах текущие ширину и высоту
        onepageScroll.winW = $(window).width();
        onepageScroll.winH = $(window).height();

        // Минимальные допустимые ширина и высота
        if(onepageScroll.winW < 640) onepageScroll.winW = 640;
        if(onepageScroll.winH < 480) onepageScroll.winH = 480;

        // Задаем ширину и высоту для слайда
        $('.chapters-wrap .chapter section').css(
            {
                width: onepageScroll.winW,
                height: onepageScroll.winH
            }
        );

        // Задаем ширину обертки глав, которая равна ширине всех глав
        // и задаем смещение, которое позвоялет просматривать нужную главу
        $('.chapters-wrap').css(
            {
                width: (onepageScroll._countChapters() * 100) + '%',
                marginLeft: -(onepageScroll.currentChapter - 1) * onepageScroll.winW
            }
        );

        // Обновление информации о позициях слайдов текущей главы
        onepageScroll._sectionsPos();
        // Переоткрытие текущего слайда (нужно для текстовых слайдов, где изменение
        // размера окна в результате приводит к возврату в начало слайда)
        onepageScroll.openScreen(onepageScroll.currentScreen, true);
    },
    // Обработчик нажатий клавиш клавиатуры
    _keyHandler: function(event)
    {
        // Если была нажата цифровая клавиша, в переменной будет сохранена цифра
        // (нужно для перехода к слайдам по номеру с клавиатуры)
        var num = Number(String.fromCharCode(event.charCode || event.which));

        // Нажата цифровая клавиша
        if(num)
        {
            // Выполнить, только если всплывающее окно не открыто
            if( ! onepageScroll._isPopupOpened())
                // Перейти к слайду по цифре
                onepageScroll.openScreen(num ? num : 10);
        }
        // Горячие клавиши не работают с нажатыми Ctrl, Alt, Shift
        else if( !event.ctrlKey && !event.altKey && !event.shiftKey)
        {
            switch(event.keyCode)
            {
                // Стрелка вверх
                case 38:
                    // Выполнить, только если всплывающее окно не открыто
                    if( ! onepageScroll._isPopupOpened())
                        // Предыдущий слайд
                        onepageScroll.prevScreen();
                    break;
                // Стрелка вниз
                case 40:
                    // Выполнить, только если всплывающее окно не открыто
                    if( ! onepageScroll._isPopupOpened())
                        // Следующий слайд
                        onepageScroll.nextScreen();
                    break;
                // Стрелка слево
                case 37:
                    // Выполнить, только если всплывающее окно не открыто
                    if( ! onepageScroll._isPopupOpened())
                        // Предыдущая глава
                        onepageScroll.prevChapter();
                    break;
                // Стрелка вправо
                case 39:
                    // Выполнить, только если всплывающее окно не открыто
                    if( ! onepageScroll._isPopupOpened())
                        // Следующая глава
                        onepageScroll.nextChapter();
                    break;
                // C
                case 67:
                    // Глобальное включение/отключение всех надписей
                    onepageScroll.toggleAllText();
                    break;
                // A
                case 65:
                    // Открытие/закрытие окна "О проекте"
                    $('footer .btn-about').trigger('click');
                    break;
                // I
                case 73:
                    // Открытие/закрытие окна оглавления
                    $('footer .btn-table-of-contents').trigger('click');
                    break;
                // F
                case 70:
                    // Включение/отключение полноэкранного режима
                    fullScreenToggle();
                    break;
                // Tab
                case 9:
                    // Нажатие Tab не должно никак обрабатываться
                    event.preventDefault();
                    break;
                // Space
                case 32:
                    // Обработка не требуется (блокировка прокрутки пробелом в Firefox)
                    break;
                // Совпадений с горячими клавишами не было найдено, завершить функцию
                default:
                    return true;
            }
        }
        // Нажата комбинация Ctrl+F (поиск)
        else if(event.ctrlKey && event.keyCode == 70)
        {
            // Особых действий не требуется, функция должна выполняться вплоть до event.preventDefault()
        }
        // Завершить функцию
        else
        {
            return true;
        }

        // Была использована одна из горячих клавиш, отменить поведение браузера по умолчанию
        event.preventDefault();
    },
    // Возвращает общее число слайдов текущей главы
    _countScreens: function()
    {
        return $('.chapters-wrap .chapter.active section').size();
    },
    // Возвращает число глав в лендинге
    _countChapters: function()
    {
        if(onepageScroll.countChapters === null)
            onepageScroll.countChapters = $('body > .chapters-wrap .chapter').size();
        return onepageScroll.countChapters;
    },
    // Обработчик события прокрутки колеса мыши
    _mousewheelHandler: function(event)
    {
        // Отменяем поведение по умолчанию (скроллинг страницы)
        event.preventDefault();
        event.stopPropagation();

        // Выполнить только если не открыто всплывающее окно и колесо не заблокировано
        if( !onepageScroll._isPopupOpened() && !onepageScroll.scrollLock)
        {
            // Колесо прокручено вверх, переход на предыдущий слайд
            if(event.deltaY > 0)
                onepageScroll.prevScreen();
            // Колесо прокручено вниз, переход на следующий слайд
            else
                onepageScroll.nextScreen();
        }
        // Если открыто всплывающее окно
        else if(onepageScroll._isPopupOpened())
        {
            // Использовать колесо для прокрутки контента в окне
            // Без этой функции колесо не будет прокручивать контент, т.е. поведение по дефолту отменено
            $('.popup:visible .scroll').each(function()
            {
                var pos = $(this).scrollTop();

                pos += (event.deltaY > 0) ? -200 : 200;
                if(pos < 0) pos = 0;

                $(this).stop(true).animate({ scrollTop: pos }, 300);
            });
        }

        return false;
    },
    // Обработчик жестов (для сенсорных устройств)
    _swipeHandler: function()
    {
        $('body').swipe(
            {
                swipe: function(event, direction, distance, duration, fingerCount)
                {
                    // Если открыто всплывающее окно, не реагировать на жест
                    if(onepageScroll._isPopupOpened()) return;

                    switch(direction)
                    {
                        // Предыдущий слайд
                        case 'down': onepageScroll.prevScreen(); break;
                        // Следующий слайд
                        case 'up': onepageScroll.nextScreen(); break;
                        // Предыдущая глава
                        case 'right': onepageScroll.prevChapter(); break;
                        // Следующая глава
                        case 'left': onepageScroll.nextChapter(); break;
                    }
                }
            }
        );
    },
    // Обработчик кликов мышью
    _mousedownHandler: function(event)
    {
        // Нажатие средней кнопки мыши
        if(event.which === 2)
            // Блокируем
            event.preventDefault();
    },
    // Рендер навигационных цифровых кнопок на нижней панели
    _navRender: function()
    {
        // HTML-код навигационных кнопок
        var navHTML = '';
        // Число слайдов в главе
        var countScreens = onepageScroll._countScreens();

        // Генерим HTML навигации по слайдам
        for(var i = 1; i <= countScreens; ++i)
        {
            navHTML += '<li' + ((i == onepageScroll.currentScreen) ? ' class="active"' : '') + '>';
            navHTML += i + '</li>';
        }

        // Выводим HTML кнопок на панель
        $('footer nav ul').html(navHTML);


            // Корневой узел текущего слайда
        var $currentScreen = onepageScroll._getCurrentScreen(),
            // Узел кнопки "Подпись" на панели
            $btnText = $('footer .btn-text');

        // Если текущий слайд - фуллсайз
        if($currentScreen.hasClass('slide-fullsize'))
        {
            // Отобразить кнопку "Подпись"
            $btnText.removeClass('hidden');

            // Если на слайде уже отображена подпись
            if($currentScreen.find('.text.visible').size())
                // Выделить кнопку на панели
                $btnText.addClass('active');
            // Подпись на этом слайде не отображена
            else
                // Не выделять кнопку на панели
                $btnText.removeClass('active');
        }
        // Текущий слайд не фуллсайз и не имеет подписей
        else
        {
            // Скрыть кнопку "Подпись"
            $btnText.addClass('hidden');
        }
    },
    // Вернет true, если в данный момент открыто всплывающее окно
    _isPopupOpened: function()
    {
        return $('.popup:visible').size();
    },
    // Возвращает алиас текущей главы
    _getURLAlias: function()
    {
        return location.pathname.replace(DIR, '').replace(/\//g, '');
    },
    // Замена плагину Midnight.js, инвертирующему шапку для слайдов с белым фоном
    _midnight: function()
    {
        var $header   = $('header'),
            scrollTop = $(document).scrollTop(),
            headerH   = $header.height(),
            top,
            bottom,
            cls;

        // Проходимся по массиву с данными о слайдах
        for(var key in onepageScroll.sectionsPos)
        {
            // Положение верхней границы слайда
            top = onepageScroll.sectionsPos[key].top - headerH;
            // Положение нижней границы слайда
            bottom = onepageScroll.sectionsPos[key].bottom - headerH;
            // Тип шапки, которая должна отображаться на этом слайде
            cls = onepageScroll.sectionsPos[key].midnight;

            // Определяем над каким из слайдов сейчас находится шапка, чтобы
            // назначить ей нужный класс
            if(scrollTop >= top && scrollTop <= bottom)
                $header.removeAttr('class').addClass(cls);
        }
    },
    // Обновляет информацию о позициях слайдов текущей главы
    _sectionsPos: function()
    {
        // Массив с данными
        onepageScroll.sectionsPos = [];

        // Обходим все слайды
        $('.chapters-wrap .chapter.active section').each(function()
        {
            // Сохраняем в массиве
            var pos =
            {
                // Положение верхней границы слайда
                top: $(this).offset().top,
                // Положение нижней границы слайда
                bottom: $(this).offset().top + $(this).innerHeight(),
                // Тип шапки, которая должна отображаться на этом слайде
                midnight: $(this).data('midnight')
            };

            onepageScroll.sectionsPos.push(pos);
        });
    },
    // Возвращает узел текущего слайда
    _getCurrentScreen: function()
    {
        return $('.chapters-wrap .chapter.active .section-' + onepageScroll.currentScreen);
    },
    // Рендер кнопок переключения между главами (стрелки слева и справа на первом слайде)
    _changeСhapterRender: function(event)
    {
            // Кнопка перехода к предыдущей главе
        var $prevChapter = $('.change-chapter .prev-chapter'),
            // Кнопка перехода к следующей главе
            $nextChapter = $('.change-chapter .next-chapter'),
            // Высота панели внизу экрана
            footerHeight = $('footer').height(),
            // Ширина кнопок переключения главы
            btnWidth = $prevChapter.width(),
            // Высота хит-зоны кнопок переключения главы (панель футера не включена в хит-зону)
            hArea = onepageScroll.winH - footerHeight;


        // Перемещение мыши
        if(event.type === 'mousemove')
        {
            // Отображение кнопки предыдущей главы
            if(event.clientX < btnWidth && event.clientY < hArea)
                $prevChapter.addClass('visible');
            // Скрытие кнопки предыдущей главы
            else
                $prevChapter.removeClass('visible');

            // Отображение кнопки следующей главы
            if(event.clientX > (onepageScroll.winW - btnWidth) && event.clientY < hArea)
                $nextChapter.addClass('visible');
            // Скрытие кнопки следующей главы
            else
                $nextChapter.removeClass('visible');
        }
        // Мышь покинула окно браузера
        else if(event.type === 'mouseleave')
        {
            // Скрыть обе кнопки
            $prevChapter.removeClass('visible');
            $nextChapter.removeClass('visible');
        }
    },
    // Вернет true, если прокрутка до указанного слайда возможна
    _canScroll: function(num)
    {
        var scroll = $('.chapters-wrap .chapter-' + onepageScroll.currentChapter).data('scroll');

        return onepageScroll.videoPlayed || num == 1 || scroll;
    },

    // Инициализация
    init: function()
    {
        // Не знаю почему, но помогает от тормозов
        $(document).scrollTop(0);
        setTimeout(function()
        { $(document).scrollTop(0); }, 500);


        // Ширина и высота окна браузера
        onepageScroll.winW = $(window).width();
        onepageScroll.winH = $(window).height();

        // Алиас текущей главы
        var alias = onepageScroll._getURLAlias();

        // Получаем номер текущей главы по алиасу
        onepageScroll.currentChapter = (alias)
            ? $('.chapters-wrap .chapter[data-alias=' + alias + ']').data('num')
            : $('.chapters-wrap').data('default');

        // Переходим к нужной главе по номеру
        onepageScroll.openChapter(onepageScroll.currentChapter, true);

        onepageScroll._resizeHandler();
        onepageScroll._navRender();
        onepageScroll._midnight();

        // Только для устройств с сенсорными экранами
        if(isTouchDevice())
            // Обработка жестов
            onepageScroll._swipeHandler();

        // Вешаем обработчики событий
        $(window)
            .on('resize', onepageScroll._resizeHandler)
            .on('keydown', onepageScroll._keyHandler)
            .on('mousewheel', onepageScroll._mousewheelHandler)
            .on('mousedown', onepageScroll._mousedownHandler)
            .on('scroll', onepageScroll._midnight);

        // Клик по навигационным кнопкам на нижней панели
        $('footer nav').on('click', 'li', function()
        {
            // Если всплывающее окно не открыто
            if( ! onepageScroll._isPopupOpened())
            {
                // Перейти на слайд, соответствующий номеру кнопки
                var num = $(this).text();
                onepageScroll.openScreen(num);
            }
        });

        // Клик по кнопке перехода на предыдущий слайд
        $('footer nav .prev-screen').on('click', function()
        {
            if( ! onepageScroll._isPopupOpened())
                onepageScroll.prevScreen();
        });
        // Клик по кнопке перехода на следующий слайд
        $('footer nav .next-screen').on('click', function()
        {
            if( ! onepageScroll._isPopupOpened())
                onepageScroll.nextScreen();
        });

        // Клик по кнопкам перехода на предыдущую/следующую главу
        $('.change-chapter .prev-chapter').on('click', onepageScroll.prevChapter);
        $('.change-chapter .next-chapter').on('click', onepageScroll.nextChapter);

        // Пользователь переходит по истории в браузере
        window.addEventListener('popstate', function(event)
        {
            // Открытие соответствующей главы
            onepageScroll.openChapter(event.state.num, false, true);
        }, false);

        // Клик по кнопке закрытия подписи в слайде фуллсайз
        $('.slide-fullsize .text .close').on('click', function()
        {
            // Скрыть саму подпись
            $(this).closest('.text').removeClass('visible');
            // Снять выделение с кнопки на нижней панели
            $('footer .btn-text').removeClass('active');
        });

        // Прослушиваем события перемещения мыши для рендера кнопок переключения главы
        $(document).on('scroll mousemove mouseleave', onepageScroll._changeСhapterRender);
    },
    // Переход на указанный слайд текущей главы
    openScreen: function(num, noAnimation)
    {
        if(onepageScroll._canScroll(num))
        {
            if(num > onepageScroll._countScreens())
                num = onepageScroll._countScreens();

            onepageScroll.currentScreen = num;

            // На первом слайде у нижней панели нет черного фона
            if(num == 1)
                $('footer').addClass('transparent');
            // На остальных фон есть
            else
                $('footer').removeClass('transparent');

            // Подсчитываем высоту всех слайдов, которые расположены выше текущего
            var top = (onepageScroll.currentScreen - 1) * onepageScroll.winH;

            // Скорость анимации прокрутки страницы до следующего слайда
            var speed = noAnimation ? 0 : SCROLL_SPEED;
            // Блокируем на время прокрутки колесо мыши
            onepageScroll.scrollLock = true;

            // Прокручиваем страницу
            $('body, html')
                .stop(true)
                .animate({ scrollTop: top },
                speed,
                function()
                {
                    // Разблокируем колесо мыши
                    onepageScroll.scrollLock = false;
                }
            );

            // Рендерим навигационные кнопки на нижней панели
            onepageScroll._navRender();
            // Обновляем дату последней активности
            hideElements.updateLastActivity();
        }
    },
    // Переход на предыдущий слайд
    prevScreen: function()
    {
        // Если прокрутка невозможна, не выполнять функцию
        if( ! onepageScroll._canScroll()) return;

        // Переходить к предыдущему слайду только если перед текущим ещё могут быть слайды
        if(onepageScroll.currentScreen > 1)
            onepageScroll.openScreen(--onepageScroll.currentScreen);
    },
    // Переход на следующий слайд
    nextScreen: function()
    {
        // Если прокрутка невозможна, не выполнять функцию
        if( ! onepageScroll._canScroll()) return;

        // Переходить к следующему слайду только если текущий слайд не последний
        if(onepageScroll.currentScreen < onepageScroll._countScreens())
            onepageScroll.openScreen(++onepageScroll.currentScreen);
    },
    // Переход на указанную главу
    openChapter: function(num, noAnimation, noHistory)
    {
        if(num)
        {
            // Делаем нужную главу активной, добавив класс active
            $('.chapters-wrap .chapter-' + num)
                .addClass('active')
                .siblings('.chapter')
                .removeClass('active');

            onepageScroll.currentChapter = num;
            // Сразу открываем самый первый слайд этой главы
            onepageScroll.openScreen(1, true);

            // Скрываем кнопки переключения глав и кнопку "Смотреть"
            $('.change-chapter, .chapters-wrap .show-video').addClass('hidden-lock');
            // Скрываем лайки
            $('.chapters-wrap .social').fadeOut(300).addClass('hide-element-lock');

            // Ожидаем, пока скроются кнопки
            setTimeout(function()
            {
                // Останавливаем видео, если оно воспроизводилось
                video.stop();

                var speed = noAnimation ? 0 : SCROLL_SPEED;

                    // Общая ширина обертки всех глав
                var wrapW = onepageScroll.winW * onepageScroll._countChapters(),
                    // Смещение, позволяющее отобразить нужную главу
                    wrapMargin = -(onepageScroll.currentChapter - 1) * onepageScroll.winW;

                // Скроллим до нужной главы
                $('.chapters-wrap')
                    .css('width', wrapW)
                    .stop(true)
                    .animate(
                    { marginLeft: wrapMargin },
                    speed,
                    function()
                    {
                        // Отображаем кнопки переключения глав и кнопку "Смотреть"
                        $('.change-chapter, .chapters-wrap .show-video').removeClass('hidden-lock');
                        // Отображаем лайки
                        $('.chapters-wrap .social').fadeIn(300).removeClass('hide-element-lock');
                    }
                );

                // Узел текущей активной главы
                var $activeChapter = $('.chapters-wrap .chapter.active');

                    // Алиас текущей главы
                var alias = $activeChapter.data('alias'),
                    // Заголовок текущей главы
                    title = $activeChapter.data('title');

                // Если переход должен сохраняться в истории
                if( ! noHistory)
                {
                    // Добавляем в историю браузера запись
                    window.history.pushState({ num: num }, title, '/' + DIR + alias + '/');
                }

                // Выводим заголовок статьи в шапке страницы
                $('header .title .text').html(title);

                // Обновляем информацию о позициях слайдов главы
                onepageScroll._sectionsPos();
                // Меняем цвет шапки на необходимый
                onepageScroll._midnight();
            }, 300);
        }
    },
    // Предыдущая глава
    prevChapter: function()
    {
        if(onepageScroll.currentChapter > 1)
        {
            // Открываем предыдущую главу
            onepageScroll.openChapter(--onepageScroll.currentChapter);
        }
        else if(onepageScroll.currentChapter == 1)
        {
            // Открывает последнюю главу
            onepageScroll.openChapter(onepageScroll._countChapters())
        }
    },
    // Следующая глава
    nextChapter: function()
    {
        if(onepageScroll.currentChapter < onepageScroll._countChapters())
        {
            // Открываем следующую главу
            onepageScroll.openChapter(++onepageScroll.currentChapter);
        }
        else if(onepageScroll.currentChapter == onepageScroll._countChapters())
        {
            // Открываем первую главу
            onepageScroll.openChapter(1);
        }
    },
    // Включение/выключение подписи слайда
    toggleText: function(target)
    {
        // Узел текущего слайда
        var $currentScreen = onepageScroll._getCurrentScreen();

        // Сначала переключаем класс на кнопке нижней панели
        $(target).toggleClass('active');

        // Определяем по классу кнопки нужно ли отображать подпись
        if($(target).hasClass('active'))
            // Отображаем
            $currentScreen.find('.text').addClass('visible');
        else
            // Скрываем
            $currentScreen.find('.text').removeClass('visible');
    },
    // Глобальное включение/выключение всех подписей лендинга
    toggleAllText: function()
    {
        if( ! onepageScroll.textGlobalVisible)
        {
            // Отображаем все подписи
            $('.chapters-wrap section .text').addClass('visible');
            $('footer .btn-text').addClass('active');
            onepageScroll.textGlobalVisible = true;
        }
        else
        {
            // Скрываем все подписи
            $('.chapters-wrap section .text').removeClass('visible');
            $('footer .btn-text').removeClass('active');
            onepageScroll.textGlobalVisible = false;
        }
    }
};

// Скрывает элементы на странице при отсутствии активности со стороны пользователя
var hideElements =
{
    // Временная метка последней активности
    lastActivity: 0,

    lastClientX: null,
    lastClientY: null,

    // Обновление временной метки последней активности
    updateLastActivity: function()
    {
        hideElements.lastActivity = new Date().getTime();
    },
    // Инициализация
    init: function()
    {
        hideElements.updateLastActivity();

        // При перемещении мыши метка последней активности будет обновляться
        $(document).on('mousemove', function(event)
        {
            var clientX = event.clientX || event.pageX;
            var clientY = event.clientY || event.pageY;

            if(hideElements.lastClientX === null && hideElements.lastClientY === null)
            {
                hideElements.lastClientX = clientX;
                hideElements.lastClientY = clientY;
            }
            else
            {
                if(hideElements.lastClientX != clientX || hideElements.lastClientY != clientY)
                {
                    hideElements.lastClientX = clientX;
                    hideElements.lastClientY = clientY;

                    hideElements.updateLastActivity();
                }
            }
        });

        // Каждую четверть секунды проверяем временную метку и если с момента последней
        // активности (перемещение курсора мыши) прошло более 2 секунд, скрываем необходимые
        // элементы
        setInterval(function()
        {
            // Текущее время
            var currentTime = new Date().getTime();

            // Скрываемые элементы
            var $nodes  = $('header, .hide-element'),
                $footer = $('footer');

            // Разница во времени превышает 2 секунды
            if((currentTime - hideElements.lastActivity) > 2000)
            {
                // Скрываем элементы
                $nodes.stop(true, true).fadeOut(500);
                $footer.addClass('hidden');
            }
            else
            {
                // Отображаем элементы (элементы с классом hide-element-lock не будут
                // отображены, пока этот класс не будет удален)
                $nodes.filter(':not(.hide-element-lock)').stop(true, true).fadeIn(250);
                $footer.removeClass('hidden');
            }
        }, 250);
    }
};

// Прокрутка страницы наверх
var scrollTop =
{
    // Узел кнопки
    $node: null,

    // Рендер кнопки
    _render: function(event)
    {
            // Текущее положение прокруки страницы
        var top  = $(document).scrollTop(),
            // Высота окна браузера
            winH = $(window).height();

        // Если текущий слайд - галерея
        if(scrollTop._gallerySlideCheck())
        {
            // Скрываем кнопку
            scrollTop.$node.removeClass('visible');

            // Дальнейший код должен выполняться только один раз, когда кнопка ещё не
            // скрыта полностью
            if( !scrollTop.$node.hasClass('hidden') && !scrollTop.t)
            {
                // Ждем, пока кнопка на экране плавно погаснет
                scrollTop.t = setTimeout(function()
                {
                    // И убираем её полностью, чтобы она не мешала работе с галереей
                    scrollTop.$node.addClass('hidden');
                    scrollTop.t = null;
                }, 200);
            }
        }
        // Текущий слайд не галерея
        else
        {
            // Перемещение мыши
            if(event.type === 'mousemove')
            {
                // Размер активной зоны зависит от ширины окна
                var y = (onepageScroll.winW > 1280) ? 360 : 220;

                // Курсор в верхней области окна, отображаем кнопку
                if(event.clientY < y)
                    scrollTop.$node.addClass('visible');
                // Курсор вне хит-зоны кнопки, скрываем её
                else
                    scrollTop.$node.removeClass('visible');
            }
            // Прокрутка окна
            else if(event.type === 'scroll')
            {
                // Страница прокручена до самого верха, блокируем кнопку полностью, чтобы не мешала
                if(top < 100)
                    scrollTop.$node.addClass('hidden');
                // Страница прокручена менее чем на один слайд, скрыть кнопку
                else if(top < winH)
                    scrollTop.$node.removeClass('visible');
                // Страница прокручена до второго слайда и более, можно отображать кнопку
                else
                    scrollTop.$node.removeClass('hidden');
            }
            // Курсор покинул окно браузера
            else if(event.type === 'mouseleave')
            {
                // Скрыть конопку
                scrollTop.$node.removeClass('visible');
            }
        }
    },
    // Проверка, является ли текущий слайд галереей (вернет true/false)
    _gallerySlideCheck: function()
    {
        var $curScr = $('.chapters-wrap .chapter.active .section-' + onepageScroll.currentScreen);
        return $curScr.hasClass('slide-gallery');
    },

    // Инициализация
    init: function()
    {
        // Узел кнопки
        scrollTop.$node = $('.scroll-top');
        // При клике на кнопку, прокрутить страницу наверх к первому слайду
        scrollTop.$node.on('click', scrollTop.scroll);

        // Перерендериваем кнопку при перемещении мыши
        $(document).on('scroll mousemove mouseleave', scrollTop._render);
    },
    // Прокрутка к самому первому слайду текущей главы
    scroll: function()
    {
        onepageScroll.openScreen(1);
    }
};

// Галерея
var gallery =
{
    // Инициализация
    init: function()
    {
        // Клик по превьюшке, отображение выбранной картинки
        $('.gallery .prev-images img').on('click', function()
        {
            var img = $(this).data('image') - 1;

            var $gallery     = $(this).closest('.gallery'),
                $bigImage    = $gallery.find('.big-image .images-wrap img:eq('+img+')'),
                $controls    = $gallery.find('.controls'),
                $description = $gallery.find('.descriptions .description:eq('+img+')');

            $controls.addClass('hidden');
            $description.siblings().stop(true).fadeOut(400);
            $bigImage.siblings('img:visible').stop(true).fadeOut(400, function()
            {
                $controls.removeClass('hidden');
                $bigImage.stop(true).fadeIn(400);
                $description.stop(true).fadeIn(400);
            });

            $(this).addClass('active').siblings('img').removeClass('active');
        });

        // Обработчики нажатий кнопок "Назад" и "Вперед"
        $('.gallery .big-image .controls .next-image').on('click', gallery.next);
        $('.gallery .big-image .controls .prev-image').on('click', gallery.prev);
    },
    // Переход к следующей картинке
    next: function()
    {
            // Корневой узел текущей галереи
        var $gallery = $(this).closest('.gallery'),
            // Следующее изображение
            $next = $gallery.find('.prev-images img.active').next();

        // Если следующего изображения нет, перейти снова к первому
        if( ! $next.size()) $next = $gallery.find('.prev-images img').first();

        $next.trigger('click');
    },
    // Переход к предыдущей картинке
    prev: function()
    {
            // Корневой узел текущей галереи
        var $gallery = $(this).closest('.gallery'),
            // Предыдущее изображение
            $prev = $gallery.find('.prev-images img.active').prev();

        // Если предыдущего изображения нет, перейти к последнему
        if( ! $prev.size()) $prev = $gallery.find('.prev-images img').last();

        $prev.trigger('click');
    }
};

// Плеер видео
var video =
{
    el: null,
    $sound: null,
    $controls: null,
    $showVideo: null,

    sourceRender: false,


    // Рендеринг полосы поиска
    _seekingBarRender: function()
    {
        var percent = 100 / (video.el.duration / video.el.currentTime);

        video.$controls
             .find('.seeking-bar .progress')
             .css('width', percent + '%');
    },
    // Вставляет теги <source> в <video>, используя data
    _videoTagRender: function()
    {
        if( ! video.sourceRender)
        {
            $('video').each(function()
            {
                var webm = $(this).data('webm'),
                    mp4  = $(this).data('mp4');

                var sourceHTML = (Modernizr.video.webm)
                    ? '<source src="' + webm + '" type="video/webm">'
                    : '<source src="' + mp4 + '">';

                $(this).prepend(sourceHTML);
            });

            video.sourceRender = true;
        }
    },

    // Инициализация
    init: function()
    {
        // Элементы управления видео
        video.$controls = $('.video-controls');
        // Кнопка вкл/выкл звука
        video.$sound = $('footer .sound');
        // Кнопка "Смотреть"
        video.$showVideo = $('.slide-video .button.show-video').not('.next-slide');
		
		// Кнопка "Смотреть фильм"
        var $showMovie = $('.show-movie');

        // Если браузер не поддерживает SVG, вывести надписи на кнопках простым текстом
        if( ! Modernizr.svg)
		{
            video.$showVideo.text('Смотреть');
			$showMovie.text('Смотреть фильм');
		}

        // Клик по кнопке "Смотреть" на первом слайде
        video.$showVideo.on('click', function()
        {
            var $section = $(this).closest('section'),
                $state_1 = $section.find('.state-1'),
                $state_2 = $section.find('.state-2');

            // Скрываем кнопку "Смотреть", заголовок, лайки
            $state_1.fadeOut(400);
            // Отображаем контейнер с видео
            $state_2.delay(400).fadeIn(400, video.play);
        });

        // Клик по самому видео
        $('.slide-video video').on('click', video.pauseToggle);

        // Клик по полосе поиска
        $('.video-controls .seeking-bar').on('click', function(event)
        {
            var posX = event.pageX - $(this).offset().left,
                maxX = $(this).width();

            var time = 1 / (maxX / posX) * video.el.duration;

            video.seek(time);
        });
    },
    // Вопроизведение видео текущей главы
    play: function()
    {
        video._videoTagRender();

        // Флаг, что видео было запущено
        onepageScroll.videoPlayed = true;

        // Видео текущей главы
        video.el = $('.chapters-wrap .chapter.active .slide-video video').get(0);

        if(video.el)
        {
            // Костыль для Opera в виде задержки вызова метода
            var playDelay = (browser.opera()) ? 150 : 0;
            // Воспроизведение видео
            setTimeout(function()
            { video.el.play() }, playDelay);

            // Перерендеринг полосы поиска во время просмотра видео
            $(video.el).on('timeupdate', video._seekingBarRender);

            // Отображение элементов управления видео
            video.$controls.addClass('visible');

            // Отображение кнопки звука
            video.$sound.fadeIn(300);

            // Изменение иконки кнопки звука
            if(video.el.muted) video.$sound.addClass('muted');
            else video.$sound.removeClass('muted');
        }
    },
    // Пауза видео, которое воспроизводится в настоящий момент
    pause: function()
    {
        if(video.el)
        {
            // Ставим видео на паузу
            video.el.pause();

            // Костыль для Opera в виде задержки вызова метода
            var playDelay = (browser.opera()) ? 150 : 0;
            // Остановка видео
            setTimeout(function()
            { video.el.pause() }, playDelay);

            $(video.el).off('timeupdate', video._seekingBarRender);

            // Скрываем элементы управления видео
            video.$controls.removeClass('visible');

            // Скрываем кнопку звука
            video.$sound.fadeOut(300);
        }
    },
    // Пауза/воспроизведение видео
    pauseToggle: function()
    {
        if(video.el)
        {
            if( ! video.el.paused)
                video.pause();
            else
                video.play();
        }
    },
    // Полная остановка видео с отображением кнопки "Смотреть" и лайков
    // (для переключения глав)
    stop: function()
    {
        // Ставим видео на паузу
        video.pause();

        var $state_2 = $('.chapters-wrap .chapter .state-2:visible'),
            $state_1 = $state_2.closest('section').find('.state-1');

        // Скрываем видео
        $state_2.fadeOut(300);
        // Сбрасываем положение полосы поиска
        $('.video-controls .seeking-bar .progress').css('width', 0);

        // Отображаем кнопки
        $state_1.delay(300).fadeIn(300);
    },
    // Переход на указанное время (в секундах)
    seek: function(time)
    {
        video.el.currentTime = time;
    },
    // Вкл/выкл звука видео
    muteToggle: function()
    {
        if(video.el)
        {
            // Переключение режима (вкл/выкл звук)
            video.el.muted = !video.el.muted;

            // Изменение иконки кнопки звука
            if(video.el.muted) video.$sound.addClass('muted');
            else video.$sound.removeClass('muted');
        }
    }
};



$(function()
{
    // Если браузер не поддерживает SVG, вывести надпись прелоадера простым текстом
    if( ! Modernizr.svg)
        $('#preloader-content .pre-main-wrap').text('Послание Тайрона');

    // Инициализация прелоадера
    $('body').jpreLoader(
        {
            splashID: '#preloader-content',
            // Автоматически отображать страницу после загрузки
            autoClose: true,
            // Будет вызвана при завершении загрузки
            onComplete: function()
            {
                // Начинаем скрытие элементов только после того как страница будет загружена и отображена
                hideElements.init();
            }
        }
    );

    // Наведение курсора на названия глав во всплывающем окне оглавления
    $('.popup.popup-table-of-contents .chapters li a')
        // Курсор навели
        .on('mouseover', function()
        {
            // Получаем номер главы, на которую был наведен курсор
            var chapter = $(this).parent().data('chapter');
            // Отображаем метку на карте, соответствующую этой главе
            $('.popup.popup-table-of-contents .map .chapter-' + chapter).addClass('visible');
        }
    )
        // Курсор убрали
        .on('mouseout', function()
        {
            // Скрываем метку на карте
            $('.popup.popup-table-of-contents .map .label:visible').removeClass('visible');
        }
    );

    // Инициализация компонентов страницы
    onepageScroll.init();
    scrollTop.init();
    gallery.init();
    video.init();

    // Если браузер IE любой версии, добавить класс тегу <body>
    if(browser.ie()) $('body').addClass('ie');

    // Переход на второй слайд по клику на кнопке "смотреть" в первой главе
    $('.chapter-1 .section-1 .show-video.next-slide').on('click', function()
    {
        onepageScroll.openScreen(2);
    });

    // Клик по активной точке
    $('.tip').on('click', function()
    {
        $('footer .btn.signature').trigger('click');
    });
});