$(function()
{
    var $body = $('body');

    $(window).on('resize', function()
    {
        var width = $(window).width();

        var className = (width < 641) ? 'w640' : (width < 981) ? 'w1024' : (width < 1281) ? 'w1280' : (width < 1601 ? 'w1600' : 'w1920');

        $body.removeClass('w640 w1024 w1280 w1600 w1920');
        $body.addClass(className);
    }).trigger('resize');
});