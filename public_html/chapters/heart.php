<div class="chapter chapter-2" data-alias="heart" data-title="Сердце мира" data-num="2" data-scroll="false">
	<section class="section-1 slide-video" data-midnight="white">
        <div class="state-1">
            <h1>Сердце мира</h1>
            <div class="show-video button button-green">
                <img src="img/watch.svg">
            </div>
            <div class="social">
                <div class="special-likes">
                    <div class="special-block">
                        <div class="fb-like" data-href="http://tayrona.planetpics.ru" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div>
                    </div>
                    <div class="special-block">
                        <div id="vk_like_machete" style="width: 110px !important"></div>
                        <script type="text/javascript">
                            VK.Widgets.Like("vk_like_machete", {type: "button", pageUrl: "http://tayrona.planetpics.ru"});
                        </script>
                    </div>
                    <div class="special-block">
                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://tayrona.planetpics.ru" data-text="BASE-JUMP В ПРЯМОМ ЭФИРЕ!" data-via="planetpics_ru">Tweet</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                    </div>
                    <div class="special-block">
                        <div class="g-plusone" data-href="http://tayrona.planetpics.ru"></div>
                        <script type="text/javascript">
                            (function(){
                                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                po.src = 'https://apis.google.com/js/plusone.js';
                                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                            })();
                        </script>
                    </div>
                    <div class="special-block">
                        <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'ok'}">Нравится</a>
                    </div>
                    <div class="special-block">
                        <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'mm'}">Нравится</a>
                        <script src="http://cdn.connect.mail.ru/js/loader.js" type="text/javascript" charset="UTF-8"></script>
                    </div>
                </div>
            </div>
        </div>
        <div class="state-2">
            <video data-webm="media/02/02-HELICOPTER_VP8.webm" data-mp4="media/02/02-HELICOPTER.mp4" loop>
                Если видео не воспроизводится, вы можете его <a href="media/02/03-INDEICY.mp4">скачать</a>.
            </video>
        </div>
    </section>
    <section class="section-2 slide-fullsize" data-midnight="white">
        <div class="text text-left">
            <div class="close"></div>
            <p>Культура индейцев коги развивалась в уникальном месте. Сьерра-Невада-де-Санта-Марта – самая высокая в мире горная гряда, находящаяся на побережье. Её можно считать зеркальным отражением нашей планеты, микрокосмосом: здесь представлены все экологические зоны и большая часть растений и животных, которые встречаются на земле.</p>
        </div>
    </section>
    <section class="section-3 slide-fullsize" data-midnight="white">
        <div class="text text-left">
            <div class="close"></div>
            <p>На этой территории обитает 628 видов птиц, 120 видов млекопитающих, 142 земноводных и рептилий и несколько видов приматов, таких как обезьяна-ревун, ночные мартышки, а также ягуары, пантеры, тигры и броненосцы.</p>
        </div>
    </section>
   
       <section class="section-4 slide-text" data-midnight="black">
        <div class="text-wrap">
            <div class="info">
                Фото: Антон Жданов<br>
                Текст: Екатерина Крылова
            </div>
            <div class="clearfix"></div>
            <p>Колумбия в представлении многих – это Карибское море, Маркес и Шакира, сальса и кумбия, плюс невероятная по своей красоте девственная природа. Рассматривая с высоты птичьего полета покрытые джунглями хребты, по которым стелется дымка облаков, задаешься вопросом: неужели именно эти меланхоличные пейзажи вдохновляли Габриеля Гарсия Маркеса, когда он писал «Сто лет одиночества»...</p>
            <p>Национальные парки «Тайрона» и «Сьерра-Невада-де-Санта-Марта» на севере Колумбии во всей своей красе предстают с воздуха: под вами как на ладони побережье Карибского моря, где когда-то швартовались конкистадоры и пираты, его пляжи и бухточки, небольшие поселения индейцев, бурные горные реки, непроходимые джунгли и заснеженные пики, на которые запрещено подниматься белому человеку. На побережье пальмовый лес соседствует с безупречными песочными пляжами,<p>
        </div>
    </section>
    <section class="section-5 slide-text" data-midnight="black">
        <div class="text-wrap">
            <p>входящими в десятку лучших в мире, а бунгало для туристов внешне как две капли воды похожи на традиционные дома индейцев.</p>
            <p>«Тайрона» – один из трех национальных парков колумбийского Карибского региона и, пожалуй, самый популярный в стране. Он оставляет далеко позади такие природоохранные зоны, как Росарио и Сан-Бернардо. Расположенный в акватории Карибского моря, в 25 километрах от Санта-Марты (департамент Магдалена), парк Тайрона тянется на 35 километров вдоль побережья, которое славится прекрасными пляжами и глубокими заливами, обрамлёнными кокосовыми рощами. Сразу за ним начинается другая заповедная зона – парк «Сьерра-Невада-де-Санта-Марта», на территории которого и находится «Потерянный город».</p>
            <p>Оба парка несколько не соответствуют типичному представлению о такого рода туристических местах. Это дикая, буйная и завораживающая территория, над которой правительство не имеет полного контроля.</p>
        </div>
    </section>
    <section class="section-6 slide-text" data-midnight="black">
        <div class="text-wrap">
            <p> Известно, что до XVI века, когда на эти земли ступила нога конкистадоров, здесь жили представители культуры Тайрона, которая объединяла несколько относительно независимых областей, управляемых вождями. Сейчас здесь проживает несколько племен коренных народов, которые считают себя потомками той цивилизации.</p>
            <p>Когда-то на месте непроходимых джунглей, которые покрывают массив Сьерра-Невада-де-Санта-Марта, находились поля и плодородные пастбища. По легенде, которую передают из уст в уста коренные жители, климатические изменения начались после того, как конкистадоры вывезли с этих земель награбленное золото, а население превратили в рабов. Вместе с золотом увезли они и дух этой земли, и боги разгневались.</p>
            <p>Индейцы считают, что создатель всего сущего, верховный бог Серанкуа, оставил им эти земли для того, чтобы они заботились о них. «Чтобы мы могли дышать, концентрироваться и кормить целый мир», – поясняет Хосе Мария Москоте Хиль, индеец из племени арсарио.</p>            
        </div>
    </section>
    <section class="section-7 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>Дома, сгруппированные вдоль берега реки Буритака, построили археологи – это реконструкция традиционных жилищ индейцев из племен коги, аруако и арсарио, которые приходят сюда выполнять свои обряды и совершать подношения. Деревни, где они живут, спрятаны в глухих джунглях. Просто так туда не пройти – требуется специальное разрешение от мамо.</p>
        </div>
    </section>
    <section class="section-8 slide-fullsize" data-midnight="white">
        <div class="text text-left">
            <div class="close"></div>
            <p>По представлениям коренных народов Сьерра-Невады, они ответственны за окружающую их природу, так как обладают некими тайными знаниями. Они называют себя «Старшими братьями», истинными опекунами планеты, и полагают, что горы Сьерра-Невада-де-Санта-Марта – это «сердце мира», которое они должны охранять.</p>
        </div>
    </section>
    <section class="section-9 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>Ни дня без ливня – таков девиз местной природы. Согласно легенде, дожди пошли на этих землях тогда, когда испанские конкистадоры позарились на золото, принадлежавшее древним культурам, и начали расхищать эти богатства – до тех пор, пока великие цивилизации не исчезли навсегда. Мать-земля разгневалась, начались дожди, и на месте полей и пастбищ на высокогорье Северо-Западных Анд выросли непроходимые джунгли. Мамо Ромуальдо полагает, что аномально влажная погода связана именно с тем, что у Тайрона забрали их золото и кварц, с помощью которого устанавливалась связь с миром духов.</p>
        </div>
    </section>
  
    <section class="section-10 slide-text" data-midnight="black">
        <div class="text-wrap">
            <p>Слова об уважении к природе, благодарности и поклонении Земле не расходятся с делом. Чуть левее от «Потерянного города» ведется строительство нескольких домиков для гидов и туристов. На их возведение идут только те деревья, которые приходится рубить при освобождении Теюны от джунглей.</p>
            <p class="quote">«В течение невероятного числа лет мы стараемся поддерживать равновесие в своих отношениях с Матерью-природой, которая нас окружает», – Хосе Мария Москоте Хиль.</span></p>
            <p>В парке «Тайрона» есть рестораны и кемпинги с хорошими душевыми и туалетами, много кафе и столовых. При этом не все бухты побережья пригодны для купания: на некоторых из них отсутствуют волнорезы – здесь властвуют сильные подводные течения. Самые уединенные пляжи и вовсе доступны только с воды – здесь нет смотрителей парка, многочисленных кафе и палаток с пивом.<p>         		
        </div>
    </section>
    <section class="section-11 slide-text" data-midnight="black">
        <div class="text-wrap">
            <p>Добраться до национального парка «Тайрона» можно по суше (из населенных пунктов Таганга или Санта-Мария, дорога до входа займет час) или по воде от Таганга на скоростном катере. В хорошую погоду имеет смысл выбрать водный вид транспорта. Полет на вертолете над территорией парка длится от полутора до трех часов, в зависимости от ветра и места вылета, цена варьируется в пределах от 700 до 12оо долларов с человека.</p>
            <p>Ежедневно к «Потерянному городу» поднимается около 15 человек. Их путь начинается и заканчивается в городке Мачете-Пелао, проходит по «Тропе Тайрона» и длится пять дней. Туристы оказываются в настоящей сельве с очень высокой влажностью воздуха, надоедливыми москитами, гигантскими деревьями, на которых растут другие деревья, удушливой жарой, бурными реками и бесконечными ниспадающими с гор потоками воды. Но награда стоит потраченных усилий – вам откроются удивительной красоты виды, которые не сможет передать ни один объектив на свете.</p>
        </div>
    </section>
    <section class="section-12 slide-gallery" data-midnight="black">
        <div class="gallery">
            <div class="prev-images">
                <img src="media/02/11-01.jpg" class="active" data-image="1">
                <img src="media/02/11-02.jpg" data-image="2">
                <img src="media/02/11-03.jpg" data-image="3">
            </div>
            <div class="big-image">
                <div class="images-wrap">
                    <img src="media/02/11-01_1.jpg">
                    <img src="media/02/11-02_1.jpg">
                    <img src="media/02/11-03_1.jpg">
                </div>
                <div class="controls">
                    <div class="prev-image"></div>
                    <div class="next-image"></div>
                </div>
            </div>
            <div class="descriptions">
                <div class="description">Несмотря на видимое спокойствие вод Карибского моря, нужно  проявлять осторожность: здесь очень сильные течения и непредсказуемые волны.</div>
                <div class="description">В течение года по тропам Потерянного города проходят сотни туристов – в основном, из Аргентины, Чили, Израиля, стран Центральной Америки и США.</div>
                <div class="description">Вертолеты с туристами на борту пролетают над Сьерра-Невада-де-Санта-Мартой, изолированным горным массивом на севере Колумбии.</div>
            </div>
        </div>
    </section>
	<section class="section-13 slide-gold-trail" data-midnight="white">
		<h1>Золотая Тропа Тайрона</h1>
		<a href="https://www.youtube.com/watch?v=WsWOzmu74N4" target="_blank" class="film-link button button-green">Смотреть фильм</a>
	</section>
</div>



