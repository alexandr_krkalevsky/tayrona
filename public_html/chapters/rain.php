<div class="chapter chapter-6" data-alias="rain" data-title="Во власти дождя" data-num="6" data-scroll="false">
    <section class="section-1 slide-video" data-midnight="white">
        <div class="state-1">
            <h1>Во власти дождя</h1>
            <div class="show-video button button-green">
                <img src="img/watch.svg">
            </div>
            <div class="social">
                <div class="special-likes">
                    <div class="special-block">
                        <div class="fb-like" data-href="http://tayrona.planetpics.ru" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div>
                    </div>
                    <div class="special-block">
                        <div id="vk_like_machete" style="width: 110px !important"></div>
                        <script type="text/javascript">
                            VK.Widgets.Like("vk_like_machete", {type: "button", pageUrl: "http://tayrona.planetpics.ru"});
                        </script>
                    </div>
                    <div class="special-block">
                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://tayrona.planetpics.ru" data-text="BASE-JUMP В ПРЯМОМ ЭФИРЕ!" data-via="planetpics_ru">Tweet</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                    </div>
                    <div class="special-block">
                        <div class="g-plusone" data-href="http://tayrona.planetpics.ru"></div>
                        <script type="text/javascript">
                            (function(){
                                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                po.src = 'https://apis.google.com/js/plusone.js';
                                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                            })();
                        </script>
                    </div>
                    <div class="special-block">
                        <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'ok'}">Нравится</a>
                    </div>
                    <div class="special-block">
                        <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'mm'}">Нравится</a>
                        <script src="http://cdn.connect.mail.ru/js/loader.js" type="text/javascript" charset="UTF-8"></script>
                    </div>
                </div>
            </div>
        </div>
        <div class="state-2">
            <video data-webm="media/06/06-RAIN_VP8.webm" data-mp4="media/06/06-RAIN.mp4" loop>
                Если видео не воспроизводится, вы можете его <a href="media/06/06-RAIN.mp4">скачать</a>.
            </video>
        </div>
    </section>
    <section class="section-2 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>В период испанского завоевания коренное население страны предположительно насчитывало 1,5–2 миллиона человек. Они счастливо жили в своем замкнутом мире до рокового 12 октября 1492 года, когда у Карибских островов появились три каравеллы Христофора Колумба. Сегодня менее 10% населения составляют чистокровные индейцы, а доля колумбийцев – потомков европейцев без примеси индейской крови – незначительна.</p>
        </div>
    </section>
    <section class="section-3 slide-fullsize" data-midnight="white">
        <div class="text text-left">
            <div class="close"></div>
            <p>Жизнь и быт коренного населения Колумбии подчинен принципам гармонии. Они сохранили свой родной язык и уникальный общинный строй. Они носят белые одежды, олицетворяющие «Великую Мать» – чистоту природы. Резиновые сапоги и станции по очистке воды – единственные признаки современной цивилизации в мире коги.</p>
            <p>Индейцы знают о жизни, здоровье и смерти практически все. Но, к сожалению, индейская медицина не оформилась в самостоятельную научную школу, поскольку испанцы не желали допускать их в какие бы то ни было сферы общественной деятельности.</p>            
        </div>
    </section>
    <section class="section-4 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>Земли Теюны очень плодородны и, к сожалению, широко используются местным населением для животноводства. Фермеры вырубают деревья, чтобы позволить пройти коровам, и сбрасывают всю землю в реки, которые из кристально чистых превращаются в коричневые. Системы очистки воды, предоставленные правительством, помогли решить проблему нехватки питьевой воды.</p>            
        </div>
    </section>
    
    <section class="section-5 slide-text" data-midnight="black">
        <div class="text-wrap">
            <div class="info">Фото: Антон Жданов<br>Текст: Екатерина Крылова</div>
            <div class="clearfix"></div>
            <p>Колумбия, как и вся Латинская Америка, находится в зоне действия Эль-Ниньо (El Nino, «мальчик» в переводе с испанского) – глобального климатического феномена, чье влияние сильнее всего ощущается ближе к Новому году. Эль-Ниньо приносит мало хорошего, вызывая бесчисленные природные катастрофы.</p>
            <p>Приход его «сестры», Ла-Нинья (La Nina, то есть «девочка»), характеризуется аномальным понижением температуры поверхности воды в центральной и восточной части тропической зоны Тихого океана. И если сравнить две климатические аномалии, то во время действия Ла-Нинья происходит гораздо меньше природных катастроф, чем во время Эль-Ниньо. «Сестра» не выходит из тени своего «брата», и ее гораздо меньше опасаются, чем её «мальчугана». Когда в регионе хозяйствует Ла-Нинья, она приносит с собой обильные циклоны, и тогда климат в этих краях делят не на сухой</p>
        </div>
    </section>
    
    <section class="section-6 slide-text" data-midnight="black">
        <div class="text-wrap">
             <p>сезон (он становится очень коротким) и сезон дождей, а на сезон коротких и длинных дождей. Ничего не поделаешь, приходится подстраиваться под ритмы природы, хотя во время сезона длинных дождей тяжело приходится всем, в том числе и археологам.</p>
             <p class="quote">«Рабочий день в сезон дождей – это время с утра до начала дождя, а потом работа встает», - археолог Сантьяго Хиральдо.</p>
            <p>Изменение климата сказывается и на выращивании сельскохозяйственных культур, что очень волнует мамо. Мамо часто молятся Матери-лягушке, чтобы она утихомирила не на шутку разгулявшуюся природную стихию. Мамо Ромуальдо показывает на один камень, который отдаленно смахивает на лягушонка: «Такие камни защищают наши сельскохозяйственные культуры от дождей», – уверяет он. Время от времени мамо собираются на террасах «Потерянного города» – священного места, где, как верят индейцы, сосредоточена вся энергия мира. Обращаясь к верховному</p>  
        </div>
    </section>

    <section class="section-7 slide-text" data-midnight="black">
        <div class="text-wrap">
        <div class="clearfix"></div>
            <p>божеству Серанкуа, мамо просят у него защиты от «мальчика» и «девочки».</p>
            <p>«Мы начинаем думать о чем-то своем, садимся рядом с этим камнем, символизирующим Мать-лягушку, и сидим три-четыре часа, – рассказывает мамо Ромуальдо. «Молитва происходит в уме, мы мысленно просим нашу Мать-Землю о том, чтобы она была к нам благосклонна, давала пропитание, наша Мать-лягушка».</p>
        </div>
    </section>     

    <section class="section-8 slide-gallery" data-midnight="black">
        <div class="gallery">
            <div class="prev-images">
                <img src="media/06/p09-2.jpg" class="active" data-image="1">
                <img src="media/06/p09-3.jpg" data-image="2">
                <img src="media/06/p09-1.jpg" data-image="3">
            </div>
            <div class="big-image">
                <div class="images-wrap">
                <img src="media/06/09-2.jpg">
                <img src="media/06/09-3.jpg">
                <img src="media/06/09-1.jpg">
                </div>
                <div class="controls">
                    <div class="prev-image"></div>
                    <div class="next-image"></div>
                </div>
            </div>
            <div class="descriptions">
                <div class="description">Во время господствования феномена Ла-Нинья в парке  парке «Сьерра-Невада-де-Санта-Марта» идут непрерывные дожди. </div>            
                <div class="description">Эти небольшие камни индейцы наделяли огромной силой – они должны защитить урожай от дождя.</div>           
                <div class="description">Системы по очистке воды размером всего 1,5 метра в диаметре, установленные на турбазах, позволили индейцам и гостям парка получать чистую питьевую воду.</div>
            </div>
        </div>
    </section>

	<section class="section-9 slide-gold-trail" data-midnight="white">
		<h1>Золотая Тропа Тайрона</h1>
		<a href="https://www.youtube.com/watch?v=WsWOzmu74N4" target="_blank" class="film-link button button-green">Смотреть фильм</a>
	</section>
</div>