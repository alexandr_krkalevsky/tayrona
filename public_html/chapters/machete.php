<div class="chapter chapter-4" data-alias="machete" data-title="Мачете-пелао" data-num="4" data-scroll="false">
    <section class="section-1 slide-video" data-midnight="white">
        <div class="state-1 ide-element">
            <h1>Мачете-Пелао</h1>
            <div class="show-video button button-green">
                <img src="img/watch.svg">
            </div>
             <div class="social">
                <div class="special-likes">
                    <div class="special-block">
                        <div class="fb-like" data-href="http://tayrona.planetpics.ru" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div>
                    </div>
                    <div class="special-block">
                        <div id="vk_like_machete" style="width: 110px !important"></div>
                        <script type="text/javascript">
                            VK.Widgets.Like("vk_like_machete", {type: "button", pageUrl: "http://tayrona.planetpics.ru"});
                        </script>
                    </div>
                    <div class="special-block">
                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://tayrona.planetpics.ru" data-text="BASE-JUMP В ПРЯМОМ ЭФИРЕ!" data-via="planetpics_ru">Tweet</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                    </div>
                    <div class="special-block">
                        <div class="g-plusone" data-href="http://tayrona.planetpics.ru"></div>
                        <script type="text/javascript">
                            (function(){
                            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                            po.src = 'https://apis.google.com/js/plusone.js';
                            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                            })();
                        </script>
                    </div>
                    <div class="special-block">
                        <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'ok'}">Нравится</a>
                    </div>
                    <div class="special-block">
                        <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'mm'}">Нравится</a>
                        <script src="http://cdn.connect.mail.ru/js/loader.js" type="text/javascript" charset="UTF-8"></script>
                    </div>
                </div>
           </div>
        </div>
        <div class="state-2">
            <video data-webm="media/04/04-MACHETE_VP8.webm" data-mp4="media/04/04-MACHETE.mp4" loop>
                <p>Если видео не воспроизводится, вы можете его <a href="media/04/04-MACHETE.mp4">скачать</a></p>
            </video>
        </div><!-- state2 -->
    </section>
    <section class="section-2 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>Мачете, как известно, – это длинный нож для уборки сахарного тростника и прорубания троп в густых зарослях. Деревня, прежде называвшаяся Мамей, неслучайно носит это название, Мачете-Пелао. Еще недавно, когда деревья были большими, мачете был главным аргументом в решении большинства спорных вопросов на территории, которая находилась под контролем наркобаронов. Сегодня местным жителям нет необходимости кому-то что-то доказывать – безопасность гарантирована государством. Жители деревни не без давления со стороны властей сменили опасную профессию на мирную и теперь поголовно работают в туризме, кто гидом, кто носильщиком, а кто – продавцом прохладительных напитков.</p>
        </div>
    </section>
    <section class="section-3 slide-fullsize" data-midnight="white">
        <div class="text text-left">
            <div class="close"></div>
            <p>Жизнь в Мачете-Пелао – будто кадры из фильмов про Колумбию: старики судачат о своем за бутылочкой пива, посреди единственной в городе улицы сам с собой играет чей-то малыш, возле забора несколько кур щиплют траву, в тени посапывает чей-то пес, в бар под грохот реггетона подтягиваются подростки…</p>
        </div>
    </section>
    <section class="section-4 slide-text" data-midnight="black">
        <div class="text-wrap">
            <div class="info">Фото: Антон Жданов<br>Текст: Екатерина Крылова</div>
            <div class="clearfix"></div>
            <p>Туристы добираются до «Потерянного города» пешком из деревни Мачете-Пелао, которая находится в 2,5 часах езды от Санта-Марты. На путь в 26 километров у туристов с гидом уходит три дня, у гида без туристов – один.</p>
            <p>Туризм – основа инфраструктуры этой деревушки, за счет него живут все, кто не занят в сфере обслуживания. Здесь нет гостиницы – для туристов это перевалочный пункт, условное начало их восхождения к истокам цивилизации Тайрона. В поселке с населением в 300 жителей всего одна улица длиной метров 200. Ведет она на центральную площадь, сразу за которой, впрочем, начинаются джунгли. Дорога от одного конца улицы до другого занимает пять минут быстрым шагом – особо не разгуляешься. Дома из кирпича и бетона похожи друг на друга как две капли воды, половина комнат в них пустует – со временем в них переедут дети ближайших родственников той семьи, которая ютится в жилой части дома.</p>
        </div>
    </section>
    <section class="section-5 slide-text" data-midnight="black">
        <div class="text-wrap">
            <p>Все мужчины и некоторые женщины трудоспособного возраста работают носильщиками, а кто поумнее – гидами. Практически никто не говорит по-английски, а потому роль экскурсовода дополнительно к роли сопровождающего доступна разве что Маррону, самому востребованному «путеводителю» городка.</p>
            <p>В городе мы насчитали пять баров. Каждое заведение представляет собой крытую площадку с бильярдными столами и едва заметным окошком в полумраке, где выдают выпивку. Из колонок любого из пяти баров всегда гремит одна и та же пластинка с десятком песен, которые повторяются по кругу и, кажется, никогда никому не надоедают. Стихает громкая музыка только часам к двум ночи.Электричество вырабатывают небольшие гидроэлектростанции, вовсю эксплуатирующие многочисленные ручьи. Однако после дождей ручьи превращаются в реки, лопасти генераторов забиваются травой, и город на непродолжительное время ремонта остается без света, музыки и… интернета. Несмотря на свои крохотные размеры, Мачете-Пелао идет в ногу со временем – у многих его жителей есть даже аккаунты в Facebook.</p>
        </div>
    </section>
    <section class="section-6 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>Самый популярный гид и главный автомеханик на деревне – Маррон. Его имя в переводе с испанского означает «коричневый». «Один мой друг однажды сказал туристам: это ваш гид, он ни черный, ни белый, он как бы Коричневый. И с тех пор все начали называть меня Маррон. Даже можно сказать, во всем мире меня именно под этим именем знают».</p>
        </div>
    </section>
    <section class="section-7 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>Большую часть времени гиды проводят вне дома, но раз в неделю, по воскресеньям, собираются вместе. Выбор развлечений невелик: бильярд на протертых кривых столах, посиделки за пивом и анисовой 20-градусной настойкой, да национальная забава под названием «тахо». Играют в неё так: в некое подобие щита, который стоит под наклоном, насыпается песок, в него втыкают капсулы с небольшим количеством пороха. Задача – с определенного расстояния попасть камнем в эту капсулу. Меткость «вознаграждается» легким взрывом. В тахо играют все жители городка, который из-за этого вечерами напоминает горячую точку.</p>
        </div>
    </section>
    <section class="section-8 slide-gallery" data-midnight="black">
        <div class="gallery">
            <div class="prev-images">
                <img src="media/04/07-01p.jpg" class="active" data-image="1">
                <img src="media/04/07-02p.jpg" data-image="2">
                <img src="media/04/07-03p.jpg" data-image="3">
                <img src="media/04/07-04p.jpg" data-image="4">
            </div>
            <div class="big-image">
                <div class="images-wrap">
                <img src="media/04/07-01.jpg">
                <img src="media/04/07-02.jpg">
                <img src="media/04/07-03.jpg">
                <img src="media/04/07-04.jpg">
                </div>
                <div class="controls">
                    <div class="prev-image"></div>
                    <div class="next-image"></div>
                </div>
            </div>
            <div class="descriptions">
                <div class="description">Бары в Мачете-Пелао – это несколько бильярдных столов и маленькая комнатка с окошком, которая выполняет роль барной стойки.</div>
                <div class="description">Индейцы из племени аруако изредка приходят в Мачете-Пелао за продовольствием.</div>
                <div class="description">Местные любят травить байки о туристах, правдивые и не очень.</div>
                <div class="description">До деревень на тропе Тайрона не доехать на машине – продовольствие доставляют с помощью мулов.</div>
            </div>
        </div>
    </section>
	<section class="section-9 slide-gold-trail" data-midnight="white">
		<h1>Золотая Тропа Тайрона</h1>
		<a href="https://www.youtube.com/watch?v=WsWOzmu74N4" target="_blank" class="film-link button button-green">Смотреть фильм</a>
	</section>
</div>