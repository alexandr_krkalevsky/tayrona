<div class="chapter chapter-7" data-alias="the-house" data-title="Дом гидов" data-num="7" data-scroll="false">
    <section class="section-1 slide-video" data-midnight="white">
        <div class="state-1">
            <h1>Дом гидов</h1>
            <div class="show-video button button-green">
                <img src="img/watch.svg">
            </div>
            <div class="social">
                <div class="special-likes">
                    <div class="special-block">
                        <div class="fb-like" data-href="http://tayrona.planetpics.ru" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div>
                    </div>
                    <div class="special-block">
                        <div id="vk_like_machete" style="width: 110px !important"></div>
                        <script type="text/javascript">
                            VK.Widgets.Like("vk_like_machete", {type: "button", pageUrl: "http://tayrona.planetpics.ru"});
                        </script>
                    </div>
                    <div class="special-block">
                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://tayrona.planetpics.ru" data-text="BASE-JUMP В ПРЯМОМ ЭФИРЕ!" data-via="planetpics_ru">Tweet</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                    </div>
                    <div class="special-block">
                        <div class="g-plusone" data-href="http://tayrona.planetpics.ru"></div>
                        <script type="text/javascript">
                            (function(){
                                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                po.src = 'https://apis.google.com/js/plusone.js';
                                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                            })();
                        </script>
                    </div>
                    <div class="special-block">
                        <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'ok'}">Нравится</a>
                    </div>
                    <div class="special-block">
                        <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'mm'}">Нравится</a>
                        <script src="http://cdn.connect.mail.ru/js/loader.js" type="text/javascript" charset="UTF-8"></script>
                    </div>
                </div>
            </div>
        </div>
        <div class="state-2">
            <video data-webm="media/07/07-GUIDES HOUSE_VP8.webm" data-mp4="media/07/07-GUIDES HOUSE.mp4" loop>
                Если видео не воспроизводится, вы можете его <a href="media/07/07-GUIDES HOUSE.mp4">скачать</a>.
            </video>
        </div>
    </section>
    <section class="section-2 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>В домиках гидов есть всё для нормальной жизни в условиях дикой природы. Они служат местом отдыха работников парка и гидов, а иногда становятся приютом для необычных туристов вроде нашей съемочной группы.</p>
        </div>
    </section>
    <section class="section-3 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>Главный дом в вертикальной плоскости разделен на две части, одна из которых расположена выше другой. В центре этой конструкции находится вход – спиралевидная лестница. Такое инженерное решение, якобы подсмотренное у индейцев, позволяет всегда держать вход сухим и оберегает его от разгула природных стихий.</p>
        </div>
    </section>
    <section class="section-4 slide-text" data-midnight="black">
        <div class="text-wrap">
            <div class="info">Фото: Антон Жданов<br>Текст: Екатерина Крылова</div>
            <div class="clearfix"></div>
            <p>Керосиновый примус, белый работающий унитаз, спутниковый телефон, современная газонокосилка – члены съемочной группы испытали культурный шок, постепенно обнаруживая следы присутствия цивилизации в затерянном в кромешных джунглях домике гидов. До ближайшей деревни отсюда три дня хода, два из которых приходится не только идти самому, но и тащить на себе все вещи – мулы по этим дорогам пройти уже не могут.</p>
            <p>Домик гидов находится чуть в стороне от места финиша всех туристов и предназначен для тех, кто так или иначе работает на благо восстановления «Потерянного города». На этой территории несколько жилых построек на сваях: в одном здании кухня, два других служат непосредственно для отдыха гидов, есть небольшой свинарник. Электричество вырабатывает генератор, черпающий энергию из расположенного по соседству водопада. Еду, горючее и все необходимые вещи приносят снизу.</p>  
        </div>
    </section>
    <section class="section-5 slide-text" data-midnight="black">
        <div class="text-wrap">
            <p>В вертикальной плоскости дом разделен на две части, одна из которых расположена выше другой. В центре этой конструкции находится вход – спиралевидная лестница. Такое инженерное решение, якобы подсмотренное у индейцев, позволяет всегда держать вход сухим и оберегает его от разгула природных стихий. На земле стоит только домик, где расположена кухня.</p>
            <p class="quote">Два месяца без присмотра – и Теюну вновь приходится отвоевывать у природы с мачете в руках.</span></p>            
            <p>Домики, как и сам «Потерянный город», находятся на территории охраняемого государством национального парка «Тайрона». Постоянно здесь живет как минимум один человек, однако останавливаются тут и археологи, а в гости нередко заходят индейцы из местного племени коги, в завсегдатаях числится и мамо Ромуальдо. Гиды останавливаются здесь когда вновь приходиться давать бой джунглям: два месяца без присмотра – и Теюну вновь приходится отвоевывать у природы с мачете в руках.</p>
        </div>
    </section>
    
    <section class="section-6 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>Все сотрудники Археологического парка так или иначе заняты в работах по реставрации территории «Потерянного города». Каждый год Фонд всемирного наследия (GHF) совместно с Колумбийским институтом антропологии и истории разрабатывает проектный план по принятию срочных мер для поддержанию комплекса.</p>
            <p>Археологам предстоит найти ответ на один из главных вопросов: благоприятно ли скажется на сохранности места статус объекта Всемирного наследия. На мировом уровне обсуждаются различные стратегии по сохранению и эффективному управлению этой территорией.</p>
        </div>
    </section>
    <section class="section-7 slide-fullsize" data-midnight="white">
        <div class="text text-left">
            <div class="close"></div>
            <p>Начальник станции Септимьо Мартинес в ноябре 2011 года занимался работами по очистке от грязи и мусора основных дренажных канав, которые были построены еще во времена Тайрона. От их чистоты зависит степень сохранности всех археологических объектов. На момент начала работ были заблокированы 90% всех каналов, местами слой грязи достигал 15 сантиметров. Застой воды на террасах приводит к наводнениям, разрушению структуры изнутри и скоплению на террасах грязи и бревен. Наводнения вызывают смещение и деформацию плит, в худших случаях из-за давления воды происходит частичное или полное разрушение террасы.</p>
        </div>
    </section>
    <section class="section-8 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>Индейцы семьями приходили на просмотр фотографий себя и своих семей. Они в этом доме частые гости – коги и аруако приходят пообщаться, выпить пива, посмотреть фильм и попросить купить немного хлопка для пошива их нехитрой одежды.</p>
        </div>
    </section>
    <section class="section-9 slide-text" data-midnight="black">
        <div class="text-wrap">
            <div class="clearfix"></div>
            <p>Жизнь здесь кипит рано утром: начиная с шести утра на террасах «Потерянного города» стригут газоны, туристы пятый раз кряду мужественно переправляются через горную реку Буритака, археологи очищают ступени Теюны от зеленого налета времени, индейцы возделывают свои земледельческие угодья.</p>
            <p>Если вышло солнце (а является оно не столь часто, как хотелось бы), то на веревках быстро развешивают чистое белье, которое высыхает в считанные минуты – температура воздуха немедля достигает 40 градусов. Стирка белья глазами белого цивилизованного человека выглядит, мягко говоря, необычно. Начать с того, что мыло здесь очень сложно смыть – вода слишком мягкая. Но женщины добиваются белизны и чистоты с маниакальным упорством. Процесс выглядит так: они приходят к реке, выбирая участок с гладкими, ровными камнями и долго выбивают о них мокрое белье, имитируя работу барабана стиральной машины. Случается, что во время стирки в заплечном мешке в это время мирно спирт её малыш – не оставлять же его одного дома.</p>
        </div>
    </section>
    <section class="section-10 slide-text" data-midnight="black">
        <div class="text-wrap">
            <div class="clearfix"></div>
            <p>К часу дня, когда начинаются дожди, все уже возвращаются в свои укрытия. В шесть вечера резко темнеет. Ужин, как правило, состоит из выращенных на этой земле продуктов. Индейцы с детишками, гиды и археологи собираются вместе и под шелест дождя коротают вечера за играми – карточной уно и Angry Birds на принесенных археологами айпадах – или за просмотром фильмов. Впрочем, хватает их ненадолго – в восемь все ложатся спать, ведь завтра рано вставать, чтобы застать время без дождя.</p>
        </div>
    </section>
    <section class="section-11 slide-gallery" data-midnight="black">
        <div class="gallery">
            <div class="prev-images">
                <img src="media/07/p07_01.jpg" class="active" data-image="1">
                <img src="media/07/p07_02.jpg" data-image="2">
                <img src="media/07/p07_03.jpg" data-image="3">
            </div>
            <div class="big-image">
                <div class="images-wrap">
                    <img src="media/07/07_01.jpg">
                    <img src="media/07/07_02.jpg">
                    <img src="media/07/07_03.jpg">
                </div>
                <div class="controls">
                    <div class="prev-image"></div>
                    <div class="next-image"></div>
                </div>
            </div>
            <div class="descriptions">
                <div class="description">Единственное средство связи с большой землей – спутниковый телефон, который может не работать из-за плохой погоды. Тогда приходится терпеливо ждать ясного неба – ведь до ближайшей деревни, Мачете, отсюда полтора дня быстрым шагом Маррона.</div>
                <div class="description">Потерянный город со всех сторон окружен дикой сельвой, и стоит на несколько месяцев оставить какой-то отрезок пути без присмотра, как он тут же зарастает, и дорогу к террасам приходится прокладывать уже с мачете в руках.</div>
                <div class="description">Хорошими отношениями с индейцами гиды обязаны Маррону. «Я познакомился с коренными жителями, когда был еще очень молод: среди них был и мамо Ромуальдо. У нас сложились хорошие отношения, но вначале, когда я работал носильщиком, все было не так».</div>
            </div>
        </div>
    </section>
	<section class="section-12 slide-gold-trail" data-midnight="white">
		<h1>Золотая Тропа Тайрона</h1>
		<a href="https://www.youtube.com/watch?v=WsWOzmu74N4" target="_blank" class="film-link button button-green">Смотреть фильм</a>
	</section>
</div>