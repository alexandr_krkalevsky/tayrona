<div class="chapter chapter-1" data-alias="message" data-title="Послание Тайрона" data-num="1" data-scroll="true">
	<section class="section-1 slide-fullsize" data-midnight="white">
        <h1>Послание Тайрона</h1>
        <div class="show-video button button-green next-slide">
            <img src="img/watch.svg">
        </div>
        <div class="social">
            <div class="special-likes">
                <div class="special-block">
                    <div class="fb-like" data-href="http://tayrona.planetpics.ru" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div>
                </div>
                <div class="special-block">
                    <div id="vk_like_machete" style="width: 110px !important"></div>
                    <script type="text/javascript">
                        VK.Widgets.Like("vk_like_machete", {type: "button", pageUrl: "http://tayrona.planetpics.ru"});
                    </script>
                </div>
                <div class="special-block">
                    <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://tayrona.planetpics.ru" data-text="BASE-JUMP В ПРЯМОМ ЭФИРЕ!" data-via="planetpics_ru">Tweet</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                </div>
                <div class="special-block">
                    <div class="g-plusone" data-href="http://tayrona.planetpics.ru"></div>
                    <script type="text/javascript">
                        (function(){
                            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                            po.src = 'https://apis.google.com/js/plusone.js';
                            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                        })();
                    </script>
                </div>
                <div class="special-block">
                    <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'ok'}">Нравится</a>
                </div>
                <div class="special-block">
                    <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'mm'}">Нравится</a>
                    <script src="http://cdn.connect.mail.ru/js/loader.js" type="text/javascript" charset="UTF-8"></script>
                </div>
            </div>
        </div>

        <div class="text text-right">
            <div class="close"></div>
            <h2>Что такое Тайрона?</h2>
            <p>Цивилизация Тайрона зародилась в горах Сьерра-Невада-де-Санта-Марта и объединила несколько территорий, управляемых независимыми вождями. Главное наследие тайронцев – город Теюна, построенный в 800 году нашей эры в непроходимых джунглях. К тому моменту возраст их цивилизации составлял восемьсот лет. Жители Теюны были искусными строителями и зодчими – они соединили поселения индейцев, разбросанные в джунглях. Однако истинным их призванием было ювелирное искусство — они оставили после себя более 50 тысяч золотых украшений.</p>
        </div>
    </section>
    <section class="section-2 slide-fullsize" data-midnight="white">
        <div class="tip tip-white" style="left: 80%; top: 40%;"></div>
        <div class="text text-right">
            <div class="close"></div>
            <p>400 лет назад, во времена конкисты и гонений со стороны испанцев, племена коги и аруако покинули процветающий город Теюну и ушли в горы, тем самым сохранив себе жизнь. Изолированные места Северо-Западных Анд дали им гарантию неприкосновенности, и они сумели сохранить свою культуру.</p>
        </div>
    </section>
    <section class="section-3 slide-fullsize" data-midnight="white">
    	<div class="text text-left">
            <div class="close"></div>
            <p>Сегодня на этих землях живут индейцы из племен коги, аруако и арсарио – потомки истребленного народа. Коги называют себя «Старшими братьями», хранителями тайного знания, которое помогает им беречь «Сердце мира» – горы Сьерра-Невады. Всех представителей белой расы они называют  «младшими братьями», и часто повторяют: мы знали, что «младшие братья» придут сюда, посетить Теюну. Они спокойно относятся к тому, что в священное для них место приезжают туристы, но просят, чтобы к городу относились с уважением.</p>
        </div>
    </section>
    <section class="section-4 slide-fullsize" data-midnight="white">
    	<div class="text text-left">
            <div class="close"></div>
            <p>Коги беспокоятся за будущее Земли: «Мать-земля – она как человек, у которого нет лишних органов. Ткани, сердце, легкие, у земли все есть. Поэтому, когда причиняется боль какому-то органу, мать-земля становится слабее, и мы все вынуждены платить за это. Вот в этом и состоит послание младшему брату, который, как знать, начнет все понимать, и мы будем едины в том, чтобы заботиться о планете, которую мы называем «мукангунагуа».</p>
        </div>
    </section>
	<section class="section-5 slide-gold-trail" data-midnight="white">
		<h1>Золотая Тропа Тайрона</h1>
		<a href="https://www.youtube.com/watch?v=WsWOzmu74N4" target="_blank" class="film-link button button-green show-movie">Смотреть фильм</a>
	</section>
</div>
