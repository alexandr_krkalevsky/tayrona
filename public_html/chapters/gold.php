<div class="chapter chapter-8" data-alias="gold" data-title="Золото теюны" data-num="8" data-scroll="false">
    <section class="section-1 slide-video" data-midnight="white">
        <div class="state-1">
            <h1>Золото Теюны</h1>
            <div class="show-video button button-green">
                <img src="img/watch.svg">
            </div>
            <div class="social">
                <div class="special-likes">
                    <div class="special-block">
                        <div class="fb-like" data-href="http://tayrona.planetpics.ru" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div>
                    </div>
                    <div class="special-block">
                        <div id="vk_like_machete" style="width: 110px !important"></div>
                        <script type="text/javascript">
                            VK.Widgets.Like("vk_like_machete", {type: "button", pageUrl: "http://tayrona.planetpics.ru"});
                        </script>
                    </div>
                    <div class="special-block">
                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://tayrona.planetpics.ru" data-text="BASE-JUMP В ПРЯМОМ ЭФИРЕ!" data-via="planetpics_ru">Tweet</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                    </div>
                    <div class="special-block">
                        <div class="g-plusone" data-href="http://tayrona.planetpics.ru"></div>
                        <script type="text/javascript">
                            (function(){
                                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                po.src = 'https://apis.google.com/js/plusone.js';
                                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                            })();
                        </script>
                    </div>
                    <div class="special-block">
                        <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'ok'}">Нравится</a>
                    </div>
                    <div class="special-block">
                        <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'mm'}">Нравится</a>
                        <script src="http://cdn.connect.mail.ru/js/loader.js" type="text/javascript" charset="UTF-8"></script>
                    </div>
                </div>
            </div>
        </div>
        <div class="state-2">
            <video data-webm="media/08/08-GOLD MUSEUM_VP8.webm" data-mp4="media/08/08-GOLD MUSEUM.mp4" loop>
                Если видео не воспроизводится, вы можете его <a href="media/08/08-GOLD MUSEUM.mp4">скачать</a>.
            </video>
        </div>
    </section>
    <section class="section-2 slide-fullsize" data-midnight="white">
        <div class="text text-bottom">
            <div class="close"></div>
            <p>В Музее золота в Боготе хранится около 55 тысяч произведений из золота, серебра и бронзы, которые были созданы на территории Латинской Америки в доколумбову эпоху: гигантские маски, диадемы, браслеты, ритуальные фигурки и символы власти из чистейшего золота, слитки, барельефы, посмертные маски, оружейные латы и даже мумии в украшениях.</p>
        </div>
    </section>
    <section class="section-3 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>Кроме изделий из золота здесь бережно хранятся необработанные изумруды общим весом в несколько килограммов. Этажи и залы разделены по темам: регионы и племена 11 народностей страны, история добычи, технологии обработки и их различия, шаманские культы… Многие экспонаты закрыты для публики – они находятся в хранилищах, куда имеют доступ только археологи и антропологи.</p>
        </div>
    </section>
    <section class="section-4 slide-text" data-midnight="black">
        <div class="text-wrap">
            <div class="info">Фото: Антон Жданов<br>Текст: Екатерина Крылова</div>
            <div class="clearfix"></div>
            <p>Своему появлению на свет Музей золота во многом обязан колумбийским этнографам и археологам братьям Висенто и Эрнесто Рестрепо. Около века назад они сумели собрать огромную коллекцию золотых изделий, принадлежавших индейцам муиски. Француз Поль Риве предложил хранить сокровища в специальном музее – так в 1939 году при Банке Республики Колумбия и был создан музей. На его создание ушло четыре года и 12 миллионов долларов.</p>
            <p>Национальной гордости Колумбии не очень идет традиционное название «музей» – экспонаты здесь не просто хранятся, а участвуют в интерактивном общении с посетителями.</p>
            <p>Последние не выходят из музея часами: от красоты и ювелирного мастерства захватывает дух, и создается впечатление, что экспонаты живые<p>
        </div>
    </section>
    <section class="section-5 slide-text" data-midnight="black">
        <div class="text-wrap">
            <p>и как будто излучают свет, чего нельзя сказать о современных творениях. Ощущения от просмотра создаются мистические – украшения будто парят в воздухе. Если вы устали рассматривать экспонаты, всегда можно выйти в широкие коридоры с панорамными окнами: пройтись, посидеть на скамейках, отдохнуть, а уж после вернуться к просмотру.</p>
            <p>Потраченное время стоит того: подобных украшений и других предметов из драгоценных металлов, собранных в одном месте, не встретить больше нигде. Поражает своей красотой огромный золотой слепок древней раковины, впечатляют золотые бусы, браслет, рыболовные крючки. Здесь можно увидеть найденный в далеком 1856 году «Золотой плот» – уникальное изделие, датируемое 1500-1200 годами до нашей эры. Он представляет собой крупную платформу из золота, над которой возвышается вождь, его, в свою очередь, окружают 12 небольших фигур.Всего в музее около 34 тысяч ювелирных изделий, и около трети из них относятся к цивилизации Тайрона. Ученые утверждают, что золотых дел</p>
        </div>
    </section>
    <section class="section-6 slide-fullsize" data-midnight="white">
        <div class="text text-bottom">
            <div class="close"></div>
            <p>В музее представлены образцы одиннадцати культур, существовавших до открытия Америки Колумбом. Самые старые золотые находки в Колумбии принадлежат эпохе Тумако. За тысячу лет до европейцев они научились делать сплав золота с платиной, хотя точка плавления платины на 1000° выше.</p>
        </div>
    </section>
    <section class="section-7 slide-text" data-midnight="black">
        <div class="text-wrap">
            <div class="clearfix"></div>
            <p>мастеров в этой культуре причисляли к божествам и наделяли сверхъестественными способностями – они обладали особым статусом в обществе и считались незаменимыми. Кстати говоря, труд историков до некоторой степени облегчает то обстоятельство, что все поселения горного массива Сьерра-Невада-де-Санта-Марта времен расцвета культуры Тайрона объединяет одна тема – «человек – летучая мышь». В музее хранятся одеяния бэтмена тех дней, диадемы, украшения для носа, рта и глаз. Фигурки летучих мышей могли символизировать мамо, который обретает мудрость в темное время суток в общении с высшими силами.</p>
            <p>Индейцы боготворили золото, которое считали частью живой природы, и в своих ритуалах и подношениях обращались к духу этого священного металла. Золото было бесценно и не передавалось по наследству. Индеец уносил свои украшения в могилу как гарантию собственного бессмертия. Не зря испанские конкистадоры верили, что легендарное Эльдорадо находится именно в Колумбии. Желтый цвет национального флага этой страны</p>
        </div>
    </section>
    <section class="section-8 slide-text" data-midnight="black">
        <div class="text-wrap">
            <p>олицетворяет золото, символизируя богатство и мощь государства.</p>
            <p class="quote">«Сейчас очень много болезней приходит. Мы продолжаем проводить наши ритуалы, совершать подношения, но это не помогает вылечить болезнь. Животные тоже умирают, их становится всё меньше», – мамо Ромуальдо.</p>            
            <p>Мамо Ромуальдо четыре раза был в Боготе и посещал музей. Он считает, что золото чувствует себя в плену, находится на положении заключенного. «Никто не беспокоится о нем, не служит ему, не проводит ритуалы и подношения», – сетует духовный лидер индейской общины коги, полагая, что священный металл недоволен. Ромуальдо чувствует его растревоженное состояние и убежден в том, что в этом и кроется причина всех катаклизмов, с которыми приходится сталкиваться индейцам.</p>
            <p>Желание мамо заботиться о золоте противоречит колумбийским</p>
        </div>
    </section>
    
    <section class="section-9 slide-text" data-midnight="black">
        <div class="text-wrap">
            <p>законам. Хуанита Саенс, сотрудница Музея золота, размышляет о драгоценностях с позиции государства: «Мы убеждаем индейцев, что музей – очень хорошее место сохранения золота. Так к нему можно относиться как к национальному достоянию. Мы рассказываем им, что эти экспонаты здесь и находятся для того, чтобы они приходили к ним столько раз, сколько они хотят, и тогда, когда они хотят».</p>
            <p>Кстати говоря, точку зрения Ромуальдо разделяют далеко не все индейцы, более того, многие из них сами приносят ювелирные изделия в музей. Но то заслуга проводимой учеными просветительской работы среди населения. К примеру, археолог Сантьяго Хиральдо придумал обучать индейцев техникам, которые использовали их предки при обработке золота. Он полагает, что так они снова обретут свое золото и смогут сами решать, что с ним делать.</p>
        </div>
    </section>
    <section class="section-10 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>Ученые предполагают, что индейцы знали о свойствах кислот и обрабатывали ими изделия. Кислота разъедала медь на поверхности и оставляла тонкий слой почти чистого золота. Для приготовления кислоты использовали мочу и настои трав. Одна из самых сложных технологий считалась технология «потерянного воска»: из глины изготавливали болванку, обмазывали её пчелиным воском, остужали и снова покрывали глиной, оставляя воронку. Глиняную форму нагревали, и через эту воронку вытекал воск, внутри формы оставалось пустое пространство. В это пространство заливали сплав золота и меди. Металл в точности повторял форму восковой заготовки. После того, как форма остывала, глину отбивали. Появлялась золотая форма.</p>
        </div>
    </section>
    <section class="section-11 slide-gallery" data-midnight="black">
        <div class="gallery">
            <div class="prev-images">
                <img src="media/08/p09-01.jpg" class="active" data-image="1">
                <img src="media/08/p09-02.jpg" data-image="2">
                <img src="media/08/p09-03.jpg" data-image="3">
                <img src="media/08/p09-04.jpg" data-image="4">
                <img src="media/08/p09-05.jpg" data-image="5">
            </div>
            <div class="big-image">
                <div class="images-wrap">
                <img src="media/08/09-01.jpg">
                <img src="media/08/09-02.jpg">
                <img src="media/08/09-03.jpg">
                <img src="media/08/09-04.jpg">
                <img src="media/08/09-05.jpg">
                </div>
                <div class="controls">
                    <div class="prev-image"></div>
                    <div class="next-image"></div>
                </div>
            </div>
            <div class="descriptions">
                <div class="description">В 2012 году Музей золота получил приглашение от Google принять участие в проекте Art Project, который в высоком разрешении рассказывает виртуальным посетителям о мировых сокровищницах.</div>
                <div class="description">Богота была основана в 1538 году испанскими конкистадорами. В центре города до сих пор сохранились образцы испанской колониальной архитектуры: старые церкви, крутые и узкие улочки.</div>
                <div class="description">Украшения были знаком божественности религиозной и политической знати. На изделиях часто красовались портреты владельцев, представителей различных каст.</div>
                <div class="description">Человек-Ягуар олицетворял для индейцев силу и смелость и, скорее всего, символизировал шамана.</div>
                <div class="description">Все экспонаты музея были извлечены из могил в течение последних 80 лет.</div>
            </div>
        </div>
    </section>
	<section class="section-12 slide-gold-trail" data-midnight="white">
		<h1>Золотая Тропа Тайрона</h1>
		<a href="https://www.youtube.com/watch?v=WsWOzmu74N4" target="_blank" class="film-link button button-green">Смотреть фильм</a>
	</section>
</div>
