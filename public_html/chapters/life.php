<div class="chapter chapter-3" data-alias="life" data-title="Быт индейцев" data-num="3" data-scroll="false">
	<section class="section-1 slide-video" data-midnight="white">
        <div class="state-1">
            <h1>Быт индейцев</h1>
            <div class="show-video button button-green">
                <img src="img/watch.svg">
            </div>
            <div class="social">
                <div class="special-likes">
                    <div class="special-block">
                        <div class="fb-like" data-href="http://tayrona.planetpics.ru" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div>
                    </div>
                    <div class="special-block">
                        <div id="vk_like_machete" style="width: 110px !important"></div>
                        <script type="text/javascript">
                            VK.Widgets.Like("vk_like_machete", {type: "button", pageUrl: "http://tayrona.planetpics.ru"});
                        </script>
                    </div>
                    <div class="special-block">
                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://tayrona.planetpics.ru" data-text="BASE-JUMP В ПРЯМОМ ЭФИРЕ!" data-via="planetpics_ru">Tweet</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                    </div>
                    <div class="special-block">
                        <div class="g-plusone" data-href="http://tayrona.planetpics.ru"></div>
                        <script type="text/javascript">
                            (function(){
                                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                po.src = 'https://apis.google.com/js/plusone.js';
                                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                            })();
                        </script>
                    </div>
                    <div class="special-block">
                        <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'ok'}">Нравится</a>
                    </div>
                    <div class="special-block">
                        <a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url=http%3A%2F%2Ftayrona.planetpics.ru" data-mrc-config="{'cm' : '1', 'sz' : '20', 'st' : '2', 'tp' : 'mm'}">Нравится</a>
                        <script src="http://cdn.connect.mail.ru/js/loader.js" type="text/javascript" charset="UTF-8"></script>
                    </div>
                </div>
            </div>
        </div>
        <div class="state-2">
            <video data-webm="media/03/03-INDEICY_VP8.webm" data-mp4="media/03/03-INDEICY.mp4" loop>
                Если видео не воспроизводится, вы можете его <a href="media/03/03-INDEICY.mp4">скачать</a>.
            </video>
        </div>
    </section>
    <section class="section-2 slide-fullsize" data-midnight="white">
        <div class="text text-right">
            <div class="close"></div>
            <p>Еще в 1992 году жители Санта-Марты и не подозревали о существовании индейцев, которые жили в горах в 37 километрах от их города. Период с 1995 по 2005 года ООН провозгласило десятилетием коренных народов земли, и о проблемах индейцев заговорили на международной арене. В 1991 году в Конституции страны прописали определение территориальной целостности коренных народов, что юридически закрепляло за ними определенные и особенные права. Территории проживания арсарио, коги и вивас получили статус Природного биосферного заповедника.</p>
        </div>
    </section>
    <section class="section-3 slide-fullsize" data-midnight="white">
        <div class="text text-left">
            <div class="close"></div>
            <p>В местах проживания коги царит атмосфера спокойствия и умиротворенности. С первых дней создания семьи в коммунах ведется коллективная духовная работа – наследники Тайрона собираются вместе для песен, медитаций и молитв.</p>
        </div>
    </section>
    <section class="section-4 slide-fullsize" data-midnight="white">
    	<div class="text text-left">
            <div class="close"></div>
            <p>Абсолютным авторитетом в индейских общинах пользуется мамо – так здесь называют жрецов и духовных наставников. Мамо решает вопросы строя общины, проводит ритуалы, освещает посевы и места для новых домов. Он связывает мир индейцев, духов и белых людей – туристов и работников парка.</p>
            <p>Чтобы стать мамо, юноши проходят специальное обучение в течение десяти-пятнадцати лет. Они постигают азы философии, психологии, медицины от своих наставников. Это время они проводят в полной темноте в специальных пещерах в Горной цепи.</p>
        </div>
    </section>
    <section class="section-5 slide-fullsize" data-midnight="white">
    	<div class="text text-bottom">
            <div class="close"></div>
            <p>В общинах индейцев Сьерра-Невады обязанности строго делятся между мужчинами и женщинами. Работа с деревом, строительство жилища – это мужская работа. А женщины занимаются собирательством, приготовлением пищи, плетением рыболовных сетей и сумок, а также уходом и уборкой кокаиновых кустов. Однако жевание кокаинового листа – это прерогатива мужчин. Женщина в культуре коги — хозяйка и покровительница земли.</p>
        </div>
    </section>
    <section class="section-6 slide-fullsize" data-midnight="white">
    	<div class="text text-right">
            <div class="close"></div>
            <p>Алехандрин добывает волокна из листа кактуса агавы. Из этих волокон индейцы делают веревки и наплечные сумки. Последние стали популярным среди туристов сувениром. Каждая сумка имеет особый орнамент – по нему можно определить социальный статус хозяина и племенную принадлежность.</p>
        </div>
    </section>
    <section class="section-7 slide-fullsize" data-midnight="white">
    	<div class="text text-bottom">
            <div class="close"></div>
            <p>Священный для индейцев коги и аруако куст коки является для них символом мужественности. Тысячелетиями листья коки использовались в религиозных целях и обрядах инициирования. Традиция жевания кокаинового листа давала индейцам витамины и позволяла вынести длительные периоды без пищи и сна.</p>
        </div>
    </section>
    <section class="section-8 slide-fullsize" data-midnight="white">
    	<div class="text text-left">
            <div class="close"></div>
            <p>С момента совершеннолетия мужчины аруако и в работе и в общении не расстаются с наркотиками. Листья кокаинового куста они постоянно держат за щекой, а внутри емкости, сделанной из тыквочки грушевидной формы и называемой попоро, находится порошок извести из жженых морских ракушек. Эту известь они достают из сосуда с помощью особой палочки и «втирают» её в комок листьев коки за щекой. Желтоватое кольцо вокруг оправы попоро получило название «калк» – это обычная слюна, смешанная с пылью оболочки. Чем больше время созерцания и глубокомысленного облизывания палки, тем больше кольцо.</p>
        </div>
    </section>
    <section class="section-9 slide-fullsize" data-midnight="white">
    	<div class="text text-bottom">
            <div class="close"></div>
            <p>Культура племен аруако и коги сильно отличается друг от друга. У аруако более развит быт, они организуют стоянки для туристов на тропе к Теюне. Они считают коги дикарями. Коги же гордятся своей религией и великой миссией – поддержанием равновесия нашего мира. Они ведут скрытный образ жизни – обычному человеку вход к жилищам коги заказан. Деревни коги спрятаны в глухих джунглях, к которым не пройти без разрешения мамо.</p>
        </div>
    </section>
    <section class="section-10 slide-fullsize" data-midnight="white">
    	<div class="text text-left">
            <div class="close"></div>
            <p>Долгое время индейцы коги не подпускали к себе посторонних. Для них изоляция была единственным способом сохранить свою культуру. И сейчас увидеть индейцев для туристов большая удача – они не любят попадаться на глаза белому человеку.</p>
        </div>
    </section>
    <section class="section-11 slide-fullsize" data-midnight="white">
    	<div class="text text-left">
            <div class="close"></div>
            <p>«Индейцы коги в Колумбии – это самая утонченная, умная, чувствительная и продвинутая цивилизация на нашей планете сегодня», – Алан Эрейра, британский режиссер и исследователь коги.</p>
        </div>
    </section>
    <section class="section-12 slide-gallery" data-midnight="black">
        <div class="gallery">
            <div class="prev-images">
                <img src="media/03/p12-01.jpg" class="active" data-image="1">
                <img src="media/03/p12-02.jpg" data-image="2">
                <img src="media/03/p12-03.jpg" data-image="3">
                <img src="media/03/p12-04.jpg" data-image="4">
            </div>
            <div class="big-image">
                <div class="images-wrap">
                <img src="media/03/12-01.jpg">
                <img src="media/03/12-02.jpg">
                <img src="media/03/12-03.jpg">
                <img src="media/03/12-04.jpg">
                </div>
                <div class="controls">
                    <div class="prev-image"></div>
                    <div class="next-image"></div>
                </div>
            </div>
            <div class="descriptions">
                <div class="description">Когда юноше исполняется 18 лет лет, ему дарят «попоро». С этого момента он считается мужчиной.</div>
                <div class="description">Такой натуральный материал – волокна кактуса агавы – индейцы используют для плетения сумок.</div>
                <div class="description">Вода в реках Сьерра-Невада очень мягкая. Чтобы отстирать бельё, местные жители подолгу выбивают его о камни.</div>
                <div class="description">Белая шапка символизируют снежные равнины священных пиков – на них мамо проводят ритуалы и церемонии.</div>
            </div>
        </div>
    </section>
	<section class="section-13 slide-gold-trail" data-midnight="white">
		<h1>Золотая Тропа Тайрона</h1>
		<a href="https://www.youtube.com/watch?v=WsWOzmu74N4" target="_blank" class="film-link button button-green">Смотреть фильм</a>
	</section>
</div>
