<?
$host = $_SERVER['HTTP_HOST'];
$dir  = ($host == 'localhost' || $host == '127.0.0.1') ? 'tayron_new/' : '';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Послание Тайрона</title>

    <base href="http://<?=$host;?>/<?=$dir;?>">

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link href="css/clear.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="css/social-likes.css" rel="stylesheet" type="text/css">
    <link href="css/jpreloader.css" rel="stylesheet" type="text/css">
    <link href="media/content.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.common.css" rel="stylesheet" type="text/css">

    <script>
        var DIR = '<?=$dir;?>';
    </script>

    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/jpreloader.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/jquery.mousewheel.min.js"></script>
    <script src="js/social-likes.min.js"></script>
    <script src="js/modernizr.custom.39290.js"></script>
    <script src="js/jquery.touchSwipe.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/jquery.responsive.js"></script>

    <!-- <script type="text/javascript" src="http://userapi.com/js/api/openapi.js"></script>
    <script type="text/javascript">
         VK.init({apiId: 2225348, onlyWidgets: true});
    </script> -->

    <script>
        document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')
    </script>
</head>
<body>

<div id="preloader-content">
    <div class="pre-main">
        <span class="pre-main-wrap"><img src="img/preloader-title.svg"><span class="text-bar"></span></span>
    </div>
    <div class="sub">о чём хотят сказать индейцы Колумбии современному миру</div>
    <div class="hot-keys">
        <div class="col">
            <div class="hint">вы можете использовать горячие клавиши:</div>
            <div class="keys keys-chapters">перемещение между главами</div>
            <div class="keys keys-scroll">следующий / предыдущий слайд</div>
            <div class="keys keys-contents">показать / скрыть оглавление</div>
        </div>
        <div class="col">
            <div class="keys keys-text">подпись фотографии</div>
            <div class="keys keys-about">показать / скрыть страницу О проекте</div>
            <div class="keys keys-sound">отключить / включить звук</div>
            <div class="keys keys-close">скрыть всплывающее окно</div>
        </div>
        <div class="clearfix"></div>
    </div><!-- hot-keys -->
</div>

<div class="popup popup-about">
    <div class="popup-content">
        <menu>
            <li class="about active" onclick="popup.about.select('about');">О проекте</li><br>
            <li class="authors" onclick="popup.about.select('authors');">Авторы</li><br>
            <li class="developers" onclick="popup.about.select('developers');">Разработчики</li>
        </menu>
        <div class="text text-about active scroll">
            <img src="img/about.jpg">
            <p>Съемочная группа познавательного ресурса Planetpics.ru побывала в национальном парке «Сьерра-Невада-де-Санта-Марта» в северо-западной части Колумбии с целью исследовать затерянный город Теюна и жизнь индейцев – потомков цивилизации Тайрона. Целью экспедиции было не только показать Теюну, но выяснить, как развитие туризма – а интерес к Колумбии со стороны путешественников год от года только растет – влияет на традиционный образ жизни индейских племен.</p>
            <p>Экспедиция длилась 18 дней и готовилась почти полгода: нужно было получить десятки разрешений и согласовать вопросы логистики. Но вот все спорные моменты улажены, четыре перелета за два дня успешно преодолены, и наш вертолет снижается посреди джунглей.</p>
            <p>На территории в десятки квадратных километров живет несколько сообществ индейцев, который ведут замкнутый образ жизни и не общаются с посторонними. На наше счастье Алехандрин и его семья из племени арсарио согласились на съемки. Наблюдать сцены из жизни индейцев было большой удачей. Ближний круг людей, с которыми они общаются, – это только гиды и ученые, проработавшие здесь много лет.</p>
            <p>Горные переходы, переправы через реки, дожди и постоянная влажность стали настоящим испытанием для съемочного оборудования. Основной фотоаппарат утонул во время переправы через горную реку, второй не выдержал влажности и соленого воздуха.</p>
        </div>
        <div class="text text-authors scroll">
            <div class="author">
                <div class="photo">
                    <img src="img/author-jdanov.png">
                </div>
                <div class="info">
                    <div class="name">Антон Жданов</div>
                    <div class="position">фотограф</div>
                    <div class="details">Исследует жизнь, создавая документальные фото-истории на интересные темы. Автор идеи и руководитель команды Planetpics.</div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="author">
                <div class="photo">
                    <img src="img/author-litovec.png">
                </div>
                <div class="info">
                    <div class="name">Сергей Литовец</div>
                    <div class="position">режиссер</div>
                    <div class="details">Член союза кинематографистов и член гильдии кинорежиссеров России. Опыт работы в качестве кинооператора на телевидении — более 150 телефильмов, телевизионных сериалов, рекламных роликов, музыкальных  клипов. Сергей участвовал в съемке более 60 документальных фильмов и более 20 короткометражных фильмов.</div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="author">
                <div class="photo">
                    <img src="img/author-kate.png">
                </div>
                <div class="info">
                    <div class="name">Екатерина Крылова</div>
                    <div class="position">автор текстов</div>
                    <div class="details">Начала свою журналистскую деятельность в 2004 году. Работала редактором в National Geographic Traveller, Russia today, журнале S7. Свободно знает испанский и регулярно путешествует с испанскими фотографами по самым глухим местам России и СНГ в поисках уникальных человеческих историй.</div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="text text-developers scroll">
            <br>
            <div class="developer" style="margin-top: 10px;">
                <div class="name">Илона Белявская</div>
                <div class="position">редактор</div>
            </div>
            <div class="developer">
                <div class="name">Саша Окунев</div>
                <div class="position">дизайнер</div>
            </div>
            <div class="developer">
                <div class="name">Дмитрий Мезенцев</div>
                <div class="position">программист</div>
            </div>
            <div class="developer">
                <div class="name">Александр Кркалевский</div>
                <div class="position">разработчик медиасервера</div>
            </div>
        </div>
        <div class="close" onclick="popup.close();"></div>
    </div>
</div>

<div class="popup popup-table-of-contents">
	<div class="popup-content">
		<div class="scroll">
			<div class="map">
                <!--
				<div class="marker chapter-1 visible"></div>
				<div class="marker chapter-2 visible"></div>
				<div class="marker chapter-3 visible"></div>
				<div class="marker chapter-4 visible"></div>
				<div class="marker chapter-5 visible"></div>
				<div class="marker chapter-6 visible"></div>
				<div class="marker chapter-7 visible"></div>
				<div class="marker chapter-8 visible"></div>
				<div class="marker chapter-9 visible"></div>
                -->
			</div>
            <ul class="chapters">
                <li data-chapter="1">Глава 1 <a href="message/">Послание &laquo;младшему брату&raquo;</a></li>
                <li data-chapter="2">Глава 2 <a href="heart/">Сердце мира</a></li>
                <li data-chapter="3">Глава 3 <a href="life/">Быт индейцев</a></li>
                <li data-chapter="3">Глава 4 <a href="machete/">Мачете пелао</a></li>
                <li data-chapter="4">Глава 5 <a href="the-path/">Путь сильных</a></li>
                <li data-chapter="5">Глава 6 <a href="rain/">Во власти дождя</a></li>
                <li data-chapter="6">Глава 7 <a href="the-house/">Дом гидов</a></li>
                <li data-chapter="7">Глава 8 <a href="gold/">Золото теюны</a></li>
                <li data-chapter="8">Глава 9 <a href="lost/">Потерянный город</a></li>
                <li data-chapter="9" class="padding"><a href="making-of/">За кадром</a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="close" onclick="popup.close();"></div>
    </div>
</div>

<div class="video-controls">
    <div class="seeking-bar">
        <div class="progress">
            <div class="pos"></div>
        </div>
    </div>
</div>

<div class="scroll-top hidden"></div>

<header>
	<div class="wrap wrap-1440">
		<a href="http://www.planetpics.ru/" class="logo"></a>
		<div class="social">
			<div class="social-likes" data-url="http://tayrona.planetpics.ru" data-title="Послание Тайрона">
				<div class="vkontakte" title="Поделиться ссылкой во ВКонтакте"></div>
				<div class="twitter" data-via="planetpics_ru" title="Поделиться ссылкой в Twitter"></div>
				<div class="facebook" title="Поделиться ссылкой в Facebook"></div>
			</div>
        </div>
        <div class="clearfix"></div>
        <div class="title">
            <div class="left-hr"></div>
            <div class="text">Потерянный город</div>
            <div class="right-hr"></div>
        </div>
    </div>
</header>

<div class="change-chapter">
    <div class="prev-chapter hide-element"></div>
    <div class="next-chapter hide-element"></div>
</div>

<div class="chapters-wrap" data-default="1">
    <? include('chapters/message.php'); ?>
    <? include('chapters/heart.php'); ?>
    <? include('chapters/life.php'); ?>
    <? include('chapters/machete.php'); ?>
    <? include('chapters/the_path.php'); ?>
    <? include('chapters/rain.php'); ?>
    <? include('chapters/the_house.php'); ?>
    <? include('chapters/gold.php'); ?>
    <? include('chapters/lost.php'); ?>
    <? include('chapters/making_off.php'); ?>
</div>

<footer>
    <div class="btn btn-popup btn-table-of-contents" onclick="popup.tableContents.show(this);">Оглавление</div>
    <div class="btn btn-popup btn-about" onclick="popup.about.show(this);">О проекте</div>
    <div class="btn btn-text hidden signature" onclick="onepageScroll.toggleText(this);">Подпись</div>

    <nav>
        <div class="prev-screen"></div>
        <ul></ul>
        <div class="next-screen"></div>
    </nav>

    <div class="full-screen" onclick="fullScreenToggle(this);"></div>
    <div class="sound" onclick="video.muteToggle();"></div>
</footer>

<script src="http://connect.facebook.net/en_US/all.js#appId=319703871378675&xfbml=1"></script>

</body>
</html>
