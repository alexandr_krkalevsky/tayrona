#!/bin/bash
# Определяем текущую дату
timestamp=`date +%Y-%m-%d-%H-%M` 

# Определяем какие каталоги будем бакапить
backupFS="/home /lib /lib64 /lost+gound /media /mnt /opt /proc /root /run /sbin /srv /sys /tmp /usr /var " 

# Определяем куда будем бакапить
backupTO="/backup/1" 
backupLog="/backup/backup.log"

# Определяем время хранения резервные копии
KeepTime=7 
 
# Записываем дату начала бэкапа 
echo "$timestamp — Start" >> $backupLog 
# Проверим наличие каталога для резеравирования
if [ -d $backupTO ]; then 
   # Удалим файлы cтарше KeepTime
   find $backupTO -maxdepth 1 -name \*.tar.gz -mtime +${KeepTime} -exec rm -f {} \; 
   for i in $backupFS #Начинаем делать резервную каталогов из backupFS
   do
      j=`expr ${i//\//-}` # Удаляем лишние символы “/”
      tar -zcvf $backupTO/`hostname`.${timestamp}.${j}.tar.gz $i  # сжимаем в архив
      echo "$timestamp — Finish" >> $backupLog # записываем дату и время окончания бэкапа
      echo "$i is done" # Выводим сообщение о завершении
   done
else
   echo "backup directory is missing...exiting"
   exit 1
fi